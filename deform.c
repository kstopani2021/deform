// deform.c: calculate sp levels and potential energy of deformed nucleus

// [2018-09-26] First attempt using Cuba library for n-dimensional integration
// [2019-08-17] Working version based on SHTNS. 7 sec/point with NMAX=11

#include <math.h>
#include <complex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdarg.h>
#include <fenv.h>
#include <sys/time.h>

#define HAVE_INLINE // inlined GSL

#include <gsl/gsl_sf_hyperg.h>
#include <gsl/gsl_sf_legendre.h>
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_sf_laguerre.h>
#include <gsl/gsl_sf_coupling.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_interp.h>
#include <gsl/gsl_sort_vector.h>

#include <fftw3.h>
#include <shtns.h>

enum nucleon_type {PROTON, NEUTRON};

const int NMAX = 11;            // maximum value of the quantum number N
int ndim;                       // size of basis
#define PI M_PI
#define UNL Unl_wikipedia
#define UNL_DERIV Unl_deriv
#define F1 f1_gradient
#define F2 f2_gradient
#define DF2DR df2dr_gradient
#define DF2DTHETA df2dtheta_gradient
#define DF2DPHI df2dphi_gradient

struct matrix {
    gsl_matrix_complex *full;
    gsl_matrix_complex *nucl;
    gsl_matrix_complex *ls;
    gsl_matrix_complex *coul;
    gsl_vector *eval;
    gsl_matrix_complex *evec;
};

// Nuclear interaction constants
double R001;     // [fm]
double alpha001; // [fm]
double U1P, U1N; // [MeV]
// Same for Coulomb interaction
double R003;     // [fm]
// Same for spin-orbit interaction
double U2;       // [MeV]
double R002;     // [fm]
double alpha002 = 0.59; // [fm]
double hbar_omega; // [MeV]
const double HC = 197.32697; // [MeV * fm]
//const double MC2_PROTON = 938.26344793; // V. N. Orlin uses this
//const double MC2_NEUTRON = 938.26344793;
const double ALPHA_FINE_STRUCTURE = 137.035999084;
//const double ALPHA_FINE_STRUCTURE = 137;
const double MC2_PROTON = 938.2720813; // [MeV]
const double MC2_NEUTRON = 939.5654133; // [MeV]
double kccc, qccc, pccc; // deformation-dependent correction of ALPHA

struct qn {
    int N, l, m, s;
};

struct qn *hbasis;

struct deform {
    double beta, gamma;
    double a0, a2;
    double a, b, c;
    double R, alpha;
    double ccc;
};

struct elem {
    struct qn bra, ket;
    //enum nucleon_type type;
    //int A, Z;
};

//#define R_MAX_INDEX 700 // up to 70 fm
//#define ALPHA_LMAX 30
#define R_MAX_INDEX 400 // up to 40 fm
#define ALPHA_LMAX 23


// partially calculated integral over THETA and PHI
struct qdouble {
    double complex cmplx[ALPHA_LMAX+1][ALPHA_LMAX+1][R_MAX_INDEX+1];
    double rval[R_MAX_INDEX];
    int npoint;
    double complex *cache;
};

struct work {
    struct deform deform;
    int A, Z;
    enum nucleon_type type;
    struct qdouble alpha; // α[λ][Δm] (Δm >= 0) for nuclear part
    struct qdouble beta;  // β[λ][Δm] (Δm >= 0) for Coulomb part
    struct qdouble gamma; // γ[λ][Δm] (Δm >= 0) for spin-orbit part
    struct qdouble mu1, mu2, mu3, mu4, mu5;
    struct qdouble kappa1, kappa2, kappa3, kappa4, kappa5, kappa6, kappa7, kappa8;
};

#define abort(...) abort123(__func__, __VA_ARGS__)

void abort123(const char *func, const char *msg, ...)
{
    va_list ap;
    printf("%s(): ", func);
    va_start(ap, msg);
    vprintf(msg, ap);
    va_end(ap);
    printf("\n");
    exit(2);
}

const char *get_param(const char *name)
{
    const char *result = getenv(name);
    if (!result)
        abort("can't find in environment: '%s'", name);
    printf("# input parameter '%s' value '%s'\n", name, result);
    return result;
}

#ifdef __APPLE__
int feenableexcept(unsigned short excepts)
{
    static fenv_t fenv;
    unsigned short new_excepts = excepts & FE_ALL_EXCEPT;
    // previous masks
    unsigned short old_excepts;

    if (fegetenv(&fenv)) {
        return -1;
    }
    old_excepts = fenv.__control & FE_ALL_EXCEPT;

    // unmask
    fenv.__control &= ~new_excepts;
    fenv.__mxcsr   &= ~(new_excepts << 7);

    return fesetenv(&fenv) ? -1 : old_excepts;
}

int fedisableexcept(unsigned short excepts)
{
    static fenv_t fenv;
    unsigned short new_excepts = excepts & FE_ALL_EXCEPT;
    // all previous masks
    unsigned short old_excepts;

    if (fegetenv(&fenv)) {
        return -1;
    }
    old_excepts = fenv.__control & FE_ALL_EXCEPT;

    // mask
    fenv.__control |= new_excepts;
    fenv.__mxcsr   |= new_excepts << 7;

    return fesetenv(&fenv) ? -1 : old_excepts;
}
#endif // __APPLE__

gsl_complex c99_to_gsl(double complex z)
{
    return gsl_complex_rect(creal(z), cimag(z));
}

double complex gsl_to_c99(gsl_complex gsl)
{
    return GSL_REAL(gsl)+I*GSL_IMAG(gsl);
}

double factorial(int n)
{
    return tgamma(n + 1);
}

double sqr(double x)
{
    return x*x;
}

double ipow(double x, int y)
{
    double ret = 1;
    for (int i = 0; i < y; i++)
        ret *= x;
    return ret;
}

// -std=gnu11 doesn't support CMPLX macro
double complex ccmplx(double real, double imag)
{
    return real+I*imag;
}

double tdelta(struct timeval *t1, struct timeval *t2)
{
    double result = 0;
    result += (double) (t2->tv_sec-t1->tv_sec);
    result += 1e-6*(t2->tv_usec-t1->tv_usec);
    return result;
}

//#pragma GCC push_options
//#pragma GCC optimize ("-O3", "unroll-loops")
double confluent_hypergeometric_fn(int alpha, double gamma, double z)
{
    assert(alpha <= 0);
    double result = 1;
    double calpha = 1;
    double cgamma = 1;
    double valpha = alpha;
    double vgamma = gamma;
//#pragma unroll 8
    for (int i = 1; i <= -alpha; i++) {
        calpha *= valpha;
        cgamma *= vgamma;
        valpha += 1;
        vgamma += 1;
        result += (calpha/cgamma)*pow(z,i)/factorial(i);
    }
    return result;
}

double confluent_hypergeometric_fn_deriv(int alpha, double gamma, double z)
{
    assert(alpha <= 0);
    double result = 0;
    double calpha = 1;
    double cgamma = 1;
    double valpha = alpha;
    double vgamma = gamma;
//#pragma unroll 8
    for (int i = 1; i <= -alpha; i++) {
        calpha *= valpha;
        cgamma *= vgamma;
        valpha += 1;
        vgamma += 1;
        result += (calpha/cgamma)*i*pow(z,i-1)/factorial(i);
    }
    return result;
}
//#pragma GCC pop_options

// Different forms of radial wavefunction of 3D isotropic harmonic oscillator
// U(n,l,r). They are different implementations of the same function through
// Laguerre polynomials and confluent hypergeometric function which must yield
// equal values.

double Unl(const struct qn *qn, const struct work *work, double r)
{
    // a = √(ℏ/mω) = √(ℏ²c²/mc²ℏω)
    // ℏω = 41 * pow(A, -1/3) MeV
    int type = work->type;
    int n = (qn->N - qn->l) / 2;
    int l = qn->l;

    double MC2 = 0;
    if (type == PROTON)
        MC2 = MC2_PROTON;
    else if (type == NEUTRON)
        MC2 = MC2_NEUTRON;
    // oscillator parameter
    double a = sqrt(HC*HC/(MC2*hbar_omega)); // [fm]
    // confluent hypergeometric function F
    //double F = gsl_sf_hyperg_1F1(-n, l+3./2., sqr(r/a));
    double F = confluent_hypergeometric_fn(-n, l+3./2., sqr(r/a));
    double c1 = (2.*tgamma(l+3./2.+n))/(a*sqr(tgamma(l+3./2.))*factorial(n));
    double c2 = pow(r/a, l)/a;
    double c3 = exp(-0.5*sqr(r/a));
    return sqrt(c1)*c2*c3*F;
}

// Normalizatino condition: ∫U(n₁,l,r)U(n₂,l,r)dr = δ(n₁,n₂)

// http://en.wikipedia.org/wiki/Quantum_harmonic_oscillator
double Unl_wikipedia(const struct qn *qn, const struct work *work, double r)
{
    // a = √(ℏ/mω) = √(ℏ²c²/mc²ℏω)
    // ℏω = 41 * pow(A, -1/3) MeV
    // ν = mω/2ℏ = 1/2a²
    int type = work->type;
    int n = (qn->N - qn->l) / 2;
    int l = qn->l;

    double MC2 = 0;
    if (type == PROTON)
        MC2 = MC2_PROTON;
    else if (type == NEUTRON)
        MC2 = MC2_NEUTRON;
    assert(MC2 > 0);
    double a = sqrt(HC * HC / (MC2 * hbar_omega)); // [fm]
    double nu = 1.0 / (2*sqr(a));
    double norm = sqrt(sqrt(2*pow(nu,3)/PI)*pow(2,n+2*l+3)*factorial(n)*pow(nu,l)/gsl_sf_doublefact(2*n + 2*l + 1));
    // Laguerre polynomial
    double L = gsl_sf_laguerre_n(n, l+0.5, 2*nu*sqr(r));
    // return r * norm * pow(r, l) * exp(-nu*sqr(r)) * L;
    return norm * pow(r, l) * exp(-nu*sqr(r)) * L;
}

// https://wikihost.nscl.msu.edu/TalentDFT/lib/exe/fetch.php?media=ho_spherical.pdf
double Unl_pdf(const struct qn *qn, const struct work *work, double r)
{
    // a = √(ℏ/mω) = √(ℏ²c²/mc²ℏω)
    // ℏω = 41 * pow(A, -1/3) МэВ
    // ν = mω/2ℏ = 1/2a²
    int type = work->type;
    int n = (qn->N - qn->l) / 2;
    int l = qn->l;

    double MC2 = 0;
    if (type == PROTON)
        MC2 = MC2_PROTON;
    else if (type == NEUTRON)
        MC2 = MC2_NEUTRON;
    double a = sqrt(HC * HC / (MC2 * hbar_omega)); // [fm]
    double norm = sqrt(pow(2, n+l+2) * factorial(n)/(sqrt(PI)*gsl_sf_doublefact(2*n+2*l+1)));
    // Laguerre polynomial
    double L = gsl_sf_laguerre_n(n, l+0.5, sqr(r/a));
    return r * norm * pow(a,-3./2.) * pow(r/a, l) * exp(-sqr(r/a)/2) * L;
}

// https://arxiv.org/pdf/nucl-th/0105070.pdf
double Unl_pdf2(const struct qn *qn, const struct work *work, double r)
{
    // a = √(ℏ/mω) = √(ℏ²c²/mc²ℏω)
    // ℏω = 41 * pow(A, -1/3) МэВ
    int type = work->type;
    int n = (qn->N - qn->l) / 2;
    int l = qn->l;

    double MC2 = 0;
    if (type == PROTON)
        MC2 = MC2_PROTON;
    else if (type == NEUTRON)
        MC2 = MC2_NEUTRON;
    double a = sqrt(HC * HC / (MC2 * hbar_omega)); // [fm]
    double lambda = 1/a;
    // confluent hypergeometric function
    //double F = gsl_sf_hyperg_1F1(-n, l + 3./2., sqr(r/a));
    double F = confluent_hypergeometric_fn(-n, l + 3./2., sqr(r/a));
    double norm = sqrt((2. * pow(lambda, 2*l+3) * tgamma(l + 3./2. + n)) /
        (sqr(tgamma(l+3./2.)) * factorial(n)));
    return r * norm * pow(r, l) * exp(-0.5*sqr(r*lambda)) * F;
}

double Unl2(const struct qn *qn, const struct work *work, double r)
{
    int type = work->type;
    int n = (qn->N - qn->l) / 2;
    int l = qn->l;

    double MC2 = 0;
    if (type == PROTON)
        MC2 = MC2_PROTON;
    else if (type == NEUTRON)
        MC2 = MC2_NEUTRON;
    double a = sqrt(HC * HC / (MC2 * hbar_omega)); // [fm]
    double x = r / a;
    double sum = 1;
    double kf = 1;

    for (int i = 1; i <= n; i++) {
        kf *= i;
        sum += pow(-1, i)*tgamma(l + 1.5)*factorial(n)
            * pow(x*x, i) / (tgamma(l + 1.5 + i) * tgamma(n - i + 1) * kf);
    }
    double c = sqrt(2. * tgamma(l + 1.5+ n)
                    / (a * sqr(tgamma(l + 1.5)) * factorial(n)));
    c *= exp(-x*x/2.) * pow(x, l+1) * sum;
    return c;
}

double Unl_deriv(const struct qn *qn, const struct work *work, double r)
{
    // a = √(ℏ/mω) = √(ℏ²c²/mc²ℏω)
    // ℏω = 41 * pow(A, -1/3) MeV
    int type = work->type;
    int n = (qn->N - qn->l) / 2;
    int l = qn->l;

    double MC2 = 0;
    if (type == PROTON)
        MC2 = MC2_PROTON;
    else if (type == NEUTRON)
        MC2 = MC2_NEUTRON;
    // oscillator parameter
    double a = sqrt(HC*HC/(MC2*hbar_omega)); // [fm]
    double F = confluent_hypergeometric_fn(-n, l+3./2., sqr(r/a));
    double DF = confluent_hypergeometric_fn_deriv(-n, l+3./2., sqr(r/a));
    double c1 = (2.*tgamma(l+3./2.+n))/(a*sqr(tgamma(l+3./2.))*factorial(n));
    double norm = sqrt(c1);
    double result = 0;
    if (l > 0)
        result += l*pow(r,l-1)/pow(a,l+1)*exp(-0.5*sqr(r/a))*F;
    result += pow(r,l)/pow(a,l+1)*(-r/sqr(a))*exp(-0.5*sqr(r/a))*F;
    result += pow(r,l)/pow(a,l+1)*exp(-0.5*sqr(r/a))*DF*2*r/sqr(a);
    return norm*result;
}

// INDEXING SCHEME OF BASIS FUNCTIONS
//
// For each N there are ⌊N/2⌋ + 1 pairs of values n = 0..⌊N/2⌋ and
// corresponding l = N - 2*n, where to each value of l
// correspond 2*l + 1 = 2*N - 4*n + 1 values of m, and for each of these
// 2 spin states +1/2 and -1/2. In total M(N) = (N+2)*(N+1) of different
// |Nlms> for each N.

int findex(int N, int n, int l, int m, int s)
{
    assert(n >= 0);
    assert(l >= 0);
    assert(abs(m) <= l);
    assert(N >= 0);
    assert(2*n + l == N);
    assert(s == 0 || s == 1);
    int b = 0;                  // first index with given N
    for (int i = 0; i < N; i++)
        b += (i+2)*(i+1);
    int c = 0;                  // offset with given l
    for (int i = 0; i < n; i++)
        c += 2*(2*(N - 2*i) + 1);
    int d = 2*(m + l);          // offset with given m
    return b + c + d + s;
}

int delta(int i, int j)
{
    if (i == j)
        return 1;
    else
        return 0;
}

// (-1)^M
double minus_one(int m)
{
    if (m%2 == 0)
        return +1;
    else
        return -1;
}

#define INT3J(a,b,c,d,e,f) \
    gsl_sf_coupling_3j(2*(a), 2*(b), 2*(c), 2*(d), 2*(e), 2*(f))
#define HAT(l) sqrt(2*(l)+1)

// Clebsch-Gordan coefficients (c) A. Edmonds 1957
double clebsch_gordan_coeff(int jj1, int mm1, int jj2, int mm2, int JJ, int MM)
{
    //   j1 j2 J         (j1-j2-M)       1
    // (         ) = (-1)         * ----------- * (j1,j2,m1,m2|J,-M)
    //   m1 m2 M                    sqrt(2*J+1)
    double v3j = gsl_sf_coupling_3j(jj1, jj2, JJ, mm1, mm2, -MM);
    int p = (jj1-jj2+MM)/2;
    assert(p*2 == jj1-jj2+MM);
    return minus_one(p)*sqrt(JJ+1)*v3j;
}

void gradient_calc(struct deform *deform, double R000, double alpha000, double theta, double phi)
{
    double a = deform->a;
    double b = deform->b;
    double c = deform->c;
    double R00 = R000/pow(a*b*c, 1.0/3.0);
    double PHI1 = sqr(sin(theta)*cos(phi)/a)+sqr(sin(theta)*sin(phi)/b)+sqr(cos(theta)/c);
    double PHI2 = sqr(cos(phi)/a)+sqr(sin(phi)/b)-1/sqr(c);
    double PHI3 = -1/sqr(a)+1/sqr(b);
    deform->alpha = deform->ccc*alpha000*sqrt(1+0.25*sqr((PHI2/PHI1)*sin(2*theta))+0.25*sqr(sin(theta)*(PHI3/PHI1)*sin(2*phi)));
    deform->R = R00/sqrt(sqr(sin(theta)*cos(phi)/a)+sqr(sin(theta)*sin(phi)/b)+sqr(cos(theta)/c));
}

// Form-factor of nuclear interaction
// Diffuseness with constant gradient
double f1_gradient(struct work *work, double r, double theta, double phi)
{
    double result;
    gradient_calc(&work->deform, R001, alpha001, theta, phi);
    double alpha = work->deform.alpha;
    double R1 = work->deform.R;
    result = 1.0/(1+exp((r-R1)/alpha));
    return result;
}

// Diffuseness proportional to radius
double f1_linear(const struct work *work, double r, double theta, double phi)
{
    double result;
    double a = work->deform.a;
    double b = work->deform.b;
    double c = work->deform.c;
    double R0 = R001/pow(a*b*c, 1.0/3.0);
    double R1 = R0/sqrt(sqr(sin(theta)*cos(phi)/a)+sqr(sin(theta)*sin(phi)/b)+sqr(cos(theta)/c));
    result = 1/(1+exp((r*R001/R1-R001)/alpha001));
    return result;
}

double f2_gradient(struct work *work, double r, double theta, double phi)
{
    double result;
    gradient_calc(&work->deform, R002, alpha002, theta, phi);
    double alpha = work->deform.alpha;
    double R2 = work->deform.R;
    result = 1.0/(1+exp((r-R2)/alpha));
    return result;
}

// Derivative of F2(R, THETA, PHI) wrt/ R for spin-orbit term
double df2dr_gradient(struct work *work, double r, double theta, double phi)
{
    gradient_calc(&work->deform, R002, alpha002, theta, phi);
    double alpha = work->deform.alpha;
    double R2 = work->deform.R;
    //double f2 = 1.0 / (1 + exp((r-R2)/alpha));
    //double result = -sqr(f2)*exp((r-R2)/alpha)/alpha;
    double e2 = exp((r-R2)/alpha);
    double result = -e2/(sqr(1+e2)*alpha);
    return result;
}

double df2dtheta_gradient(struct work *work, double r, double theta, double phi)
{
    gradient_calc(&work->deform, R002, alpha002, theta, phi);
    double alpha = work->deform.alpha;
    double R2 = work->deform.R;
    double a = work->deform.a;
    double b = work->deform.b;
    double c = work->deform.c;
    double R02 = R002/pow(a*b*c, 1.0/3.0);

    // Mathematica
    double dalpha_dtheta = (alpha002*pow(4 + pow(pow(a,2)*pow(b,2)*pow(cos(theta),2) +
          pow(c,2)*(pow(b,2)*pow(cos(phi),2) + pow(a,2)*pow(sin(phi),2))*
           pow(sin(theta),2),-2)*(pow(c,4)*pow(pow(a,2) - pow(b,2),2)*pow(sin(2*phi),2)*
           pow(sin(theta),2) + pow(pow(b,2)*pow(c,2)*pow(cos(phi),2) +
             pow(a,2)*(-pow(b,2) + pow(c,2)*pow(sin(phi),2)),2)*pow(sin(2*theta),2)),
      -0.5)*(-2*pow(pow(a,2)*pow(b,2)*pow(cos(theta),2) +
          pow(c,2)*(pow(b,2)*pow(cos(phi),2) + pow(a,2)*pow(sin(phi),2))*
           pow(sin(theta),2),-3)*(pow(c,4)*pow(pow(a,2) - pow(b,2),2)*pow(sin(2*phi),2)*
           pow(sin(theta),2) + pow(pow(b,2)*pow(c,2)*pow(cos(phi),2) +
             pow(a,2)*(-pow(b,2) + pow(c,2)*pow(sin(phi),2)),2)*pow(sin(2*theta),2))*
        (-2*cos(theta)*pow(a,2)*pow(b,2)*sin(theta) +
          2*cos(theta)*pow(c,2)*(pow(b,2)*pow(cos(phi),2) + pow(a,2)*pow(sin(phi),2))*
           sin(theta)) + pow(pow(a,2)*pow(b,2)*pow(cos(theta),2) +
          pow(c,2)*(pow(b,2)*pow(cos(phi),2) + pow(a,2)*pow(sin(phi),2))*
           pow(sin(theta),2),-2)*(2*cos(theta)*pow(c,4)*pow(pow(a,2) - pow(b,2),2)*
           pow(sin(2*phi),2)*sin(theta) +
          4*cos(2*theta)*pow(pow(b,2)*pow(c,2)*pow(cos(phi),2) +
             pow(a,2)*(-pow(b,2) + pow(c,2)*pow(sin(phi),2)),2)*sin(2*theta))))/4.;
    double dR2_dtheta = -(R02*pow(pow(c,-2)*pow(cos(theta),2) +
        (pow(a,-2)*pow(cos(phi),2) + pow(b,-2)*pow(sin(phi),2))*pow(sin(theta),2),-1.5)*
      (-2*cos(theta)*pow(c,-2)*sin(theta) +
        2*cos(theta)*(pow(a,-2)*pow(cos(phi),2) + pow(b,-2)*pow(sin(phi),2))*sin(theta))
      )/2.;

    double e2 = exp((r-R2)/alpha);
    double result = (1/sqr(1+e2))*e2*((dR2_dtheta*alpha+(r-R2)*dalpha_dtheta)/sqr(alpha));
    return result;
}

double df2dphi_gradient(struct work *work, double r, double theta, double phi)
{
    gradient_calc(&work->deform, R002, alpha002, theta, phi);
    double alpha = work->deform.alpha;
    double R2 = work->deform.R;
    double a = work->deform.a;
    double b = work->deform.b;
    double c = work->deform.c;
    double R02 = R002/pow(a*b*c, 1.0/3.0);

    // Mathematica
    double dalpha_dphi = (alpha002*pow(4 + pow(pow(a,2)*pow(b,2)*pow(cos(theta),2) +
          pow(c,2)*(pow(b,2)*pow(cos(phi),2) + pow(a,2)*pow(sin(phi),2))*
           pow(sin(theta),2),-2)*(pow(c,4)*pow(pow(a,2) - pow(b,2),2)*pow(sin(2*phi),2)*
           pow(sin(theta),2) + pow(pow(b,2)*pow(c,2)*pow(cos(phi),2) +
             pow(a,2)*(-pow(b,2) + pow(c,2)*pow(sin(phi),2)),2)*pow(sin(2*theta),2)),
      -0.5)*(-2*pow(c,2)*pow(pow(a,2)*pow(b,2)*pow(cos(theta),2) +
          pow(c,2)*(pow(b,2)*pow(cos(phi),2) + pow(a,2)*pow(sin(phi),2))*
           pow(sin(theta),2),-3)*pow(sin(theta),2)*
        (pow(c,4)*pow(pow(a,2) - pow(b,2),2)*pow(sin(2*phi),2)*pow(sin(theta),2) +
          pow(pow(b,2)*pow(c,2)*pow(cos(phi),2) +
             pow(a,2)*(-pow(b,2) + pow(c,2)*pow(sin(phi),2)),2)*pow(sin(2*theta),2))*
        (2*cos(phi)*pow(a,2)*sin(phi) - 2*cos(phi)*pow(b,2)*sin(phi)) +
       pow(pow(a,2)*pow(b,2)*pow(cos(theta),2) +
          pow(c,2)*(pow(b,2)*pow(cos(phi),2) + pow(a,2)*pow(sin(phi),2))*
           pow(sin(theta),2),-2)*(2*
           (pow(b,2)*pow(c,2)*pow(cos(phi),2) +
             pow(a,2)*(-pow(b,2) + pow(c,2)*pow(sin(phi),2)))*pow(sin(2*theta),2)*
           (2*cos(phi)*pow(a,2)*pow(c,2)*sin(phi) -
             2*cos(phi)*pow(b,2)*pow(c,2)*sin(phi)) +
          4*cos(2*phi)*pow(c,4)*pow(pow(a,2) - pow(b,2),2)*pow(sin(theta),2)*sin(2*phi))
       ))/4.;
    double dR2_dphi = -(R02*pow(pow(c,-2)*pow(cos(theta),2) +
        (pow(a,-2)*pow(cos(phi),2) + pow(b,-2)*pow(sin(phi),2))*pow(sin(theta),2),-1.5)*
      pow(sin(theta),2)*(-2*cos(phi)*pow(a,-2)*sin(phi) + 2*cos(phi)*pow(b,-2)*sin(phi))
      )/2.;

    double e2 = exp((r-R2)/alpha);
    double result = (1/sqr(1+e2))*e2*((dR2_dphi*alpha+(r-R2)*dalpha_dphi)/sqr(alpha));
    return result;
}

double df2dr_linear(const struct work *work, double r, double theta, double phi)
{
    double a = work->deform.a;
    double b = work->deform.b;
    double c = work->deform.c;
    double R02 = R002/pow(a*b*c, 1.0/3.0);
    double R2 = R02/sqrt(sqr(sin(theta)*cos(phi)/a)+sqr(sin(theta)*sin(phi)/b)+sqr(cos(theta)/c));
    //double f2 = 1.0 / (1 + exp((r*R002/R2-R002)/alpha002));
    //double result = -sqr(f2)*exp((r*R002/R2-R002)/alpha002)*R002/(R2*alpha002);
    double e2 = exp((r*R002/R2-R002)/alpha002);
    double result = -e2/sqr(1+e2)*R002/(R2*alpha002);
    return result;
}


// Charge radius for Coulomb interaction
static inline double R3(const struct work *work, double theta, double phi)
{
    double a = work->deform.a;
    double b = work->deform.b;
    double c = work->deform.c;
    double R03 = R003/pow(a*b*c, 1.0/3.0);
    double result = R03/sqrt(sqr(sin(theta)*cos(phi)/a)+sqr(sin(theta)*sin(phi)/b)+sqr(cos(theta)/c));
    return result;
}

static inline double K(const struct work *work, int lambda, double r, double theta, double phi)
{
    double r3 = R3(work, theta, phi);
    double result;
    if (r <= r3) {
        if (lambda != 2) {
            result = (2.0*lambda+1)/((lambda+3.0)*(lambda-2.0))*sqr(r)-1.0/(lambda-2.0)*pow(r,lambda)/pow(r3,lambda-2);
        } else {
            if (r < 1e-4)
                result = 0;
            else
                result = sqr(r)/5+sqr(r)*log(r3/r);
        }
    } else {
        result = 1.0/(lambda+3.0)*pow(r3,lambda+3)/pow(r,lambda+1);
    }
    return result;
}

void print_elem(const struct elem *elem)
{
    printf("<%d,%d,%d,%d|U|%d,%d,%d,%d>\n", elem->bra.N, elem->bra.l, elem->bra.m, elem->bra.s, elem->ket.N, elem->ket.l, elem->ket.m, elem->ket.s);
}

#define CACHESIGNTR \
    cache_type[NMAX+1][NMAX+1][NMAX+1][NMAX+1][2*NMAX+1][2*NMAX+1];
//             Np      lp      N       l       lambda    mdiff
double complex cache_get(void *cache, const struct elem *elem, int lambda, int mdiff)
{
    //double (*c)[NMAX+1][NMAX+1][NMAX+1][NMAX+1][2*NMAX+1][2*NMAX+1];
    typedef double complex CACHESIGNTR;
    cache_type *c = cache;
    int Np = elem->bra.N;
    int lp = elem->bra.l;
    int N = elem->ket.N;
    int l = elem->ket.l;
    assert(Np >= 0 && Np <= NMAX);
    assert(lp >= 0 && lp <= NMAX);
    assert(N >= 0 && N <= NMAX);
    assert(l >= 0 && l <= NMAX);
    assert(lambda >= 0 && lambda <= 2*NMAX);
    assert(mdiff >= 0 && mdiff <= 2*NMAX);
    return (*c)[Np][lp][N][l][lambda][mdiff];
}

void cache_set(void *cache, const struct elem *elem, int lambda, int mdiff, double complex value)
{
    typedef double complex CACHESIGNTR;
    cache_type *c = cache;
    int Np = elem->bra.N;
    int lp = elem->bra.l;
    int N = elem->ket.N;
    int l = elem->ket.l;
    assert(Np >= 0 && Np <= NMAX);
    assert(lp >= 0 && lp <= NMAX);
    assert(N >= 0 && N <= NMAX);
    assert(l >= 0 && l <= NMAX);
    assert(lambda >= 0 && lambda <= 2*NMAX);
    assert(mdiff >= 0 && mdiff <= 2*NMAX);
    (*c)[Np][lp][N][l][lambda][mdiff] = value;
}

double complex *alloc_cache()
{
    typedef double complex CACHESIGNTR;
    assert(sizeof(cache_type)/sizeof(double complex) == (NMAX+1)*(NMAX+1)*(NMAX+1)*(NMAX+1)*(2*NMAX+1)*(2*NMAX+1));
    size_t size = sizeof(cache_type);
    return malloc(size);
}

void reset_cache(double complex *cache)
{
    typedef double complex CACHESIGNTR;
    assert(sizeof(cache_type)/sizeof(double complex) == (NMAX+1)*(NMAX+1)*(NMAX+1)*(NMAX+1)*(2*NMAX+1)*(2*NMAX+1));
    size_t count = sizeof(cache_type)/sizeof(double complex);
    for (size_t i = 0; i < count; i++)
        cache[i] = ccmplx(nan(""), nan(""));
}

void make_alpha_and_beta(struct work *work)
{
    const double rstep = 0.1; // [fm]
    const double rmax = 30; // [fm]
    const int npoint = (int) round(rmax/rstep) + 1;
    assert(npoint <= R_MAX_INDEX+1);
    work->alpha.npoint = npoint;
    work->beta.npoint = npoint;
    work->gamma.npoint = npoint;
    work->mu1.npoint = npoint;
    work->mu2.npoint = npoint;
    work->mu3.npoint = npoint;
    work->mu4.npoint = npoint;
    work->mu5.npoint = npoint;
    work->kappa1.npoint = npoint;
    work->kappa2.npoint = npoint;
    work->kappa3.npoint = npoint;
    work->kappa4.npoint = npoint;
    work->kappa5.npoint = npoint;
    work->kappa6.npoint = npoint;
    work->kappa7.npoint = npoint;
    work->kappa8.npoint = npoint;

    struct timeval tbegin, tend;
    gettimeofday(&tbegin, NULL);

    printf("# begin generate ALPHA, BETA, and GAMMA arrays... ");
    fflush(stdout);

    static int first = 1;
    static shtns_cfg shtns;
    static int nlat, nphi;
    static double *vgrid;
    static double *dgrid;
    static double *kgrid[ALPHA_LMAX+1];
    static double *mu1grid, *mu3grid, *mu5grid;
    static double *kappa1grid, *kappa2grid, *kappa4grid, *kappa6grid, *kappa7grid, *kappa8grid;
    static double complex *cv;
    static double complex *cd;
    static double complex *ck[ALPHA_LMAX+1];
    static double complex *cmu1, *cmu3, *cmu5;
    static double complex *ckappa1, *ckappa2, *ckappa4, *ckappa6, *ckappa7, *ckappa8;

    if (first) {
        first = 0;
        const enum shtns_norm norm = sht_orthonormal;
        shtns = shtns_create(2*NMAX, 2*NMAX, 1, norm);
        //const enum shtns_type flags = sht_gauss|SHT_PHI_CONTIGUOUS;
        const enum shtns_type flags = sht_gauss|SHT_THETA_CONTIGUOUS;
        const int size = shtns_set_grid_auto(shtns, flags, 0, 1, &nlat, &nphi);
        printf("[nlat %d nphi %d] ", nlat, nphi);
        const int NLM = shtns->nlm;
        vgrid = fftw_malloc(NSPAT_ALLOC(shtns)*sizeof(double));
        dgrid = fftw_malloc(NSPAT_ALLOC(shtns)*sizeof(double));
        mu1grid = fftw_malloc(NSPAT_ALLOC(shtns)*sizeof(double));
        //mu2grid = fftw_malloc(NSPAT_ALLOC(shtns)*sizeof(double));
        mu3grid = fftw_malloc(NSPAT_ALLOC(shtns)*sizeof(double));
        //mu4grid = fftw_malloc(NSPAT_ALLOC(shtns)*sizeof(double));
        mu5grid = fftw_malloc(NSPAT_ALLOC(shtns)*sizeof(double));
        kappa1grid = fftw_malloc(NSPAT_ALLOC(shtns)*sizeof(double));
        kappa2grid = fftw_malloc(NSPAT_ALLOC(shtns)*sizeof(double));
        //kappa3grid = fftw_malloc(NSPAT_ALLOC(shtns)*sizeof(double));
        kappa4grid = fftw_malloc(NSPAT_ALLOC(shtns)*sizeof(double));
        //kappa5grid = fftw_malloc(NSPAT_ALLOC(shtns)*sizeof(double));
        kappa6grid = fftw_malloc(NSPAT_ALLOC(shtns)*sizeof(double));
        kappa7grid = fftw_malloc(NSPAT_ALLOC(shtns)*sizeof(double));
        kappa8grid = fftw_malloc(NSPAT_ALLOC(shtns)*sizeof(double));
        for (int i = 0; i < ALPHA_LMAX+1; i++) {
            kgrid[i] = fftw_malloc(NSPAT_ALLOC(shtns)*sizeof(double));
            ck[i] = fftw_malloc(NLM*sizeof(complex double));
        }
        cv = fftw_malloc(NLM*sizeof(complex double));
        cd = fftw_malloc(NLM*sizeof(complex double));
        cmu1 = fftw_malloc(NLM*sizeof(complex double));
        //cmu2 = fftw_malloc(NLM*sizeof(complex double));
        cmu3 = fftw_malloc(NLM*sizeof(complex double));
        //cmu4 = fftw_malloc(NLM*sizeof(complex double));
        cmu5 = fftw_malloc(NLM*sizeof(complex double));
        ckappa1 = fftw_malloc(NLM*sizeof(complex double));
        ckappa2 = fftw_malloc(NLM*sizeof(complex double));
        //ckappa3 = fftw_malloc(NLM*sizeof(complex double));
        ckappa4 = fftw_malloc(NLM*sizeof(complex double));
        //ckappa5 = fftw_malloc(NLM*sizeof(complex double));
        ckappa6 = fftw_malloc(NLM*sizeof(complex double));
        ckappa7 = fftw_malloc(NLM*sizeof(complex double));
        ckappa8 = fftw_malloc(NLM*sizeof(complex double));
    }

    for (int jr = 0; jr < npoint; jr++) {
        const double r = jr*rstep;
        work->alpha.rval[jr] = r;
        work->beta.rval[jr] = r;
        work->gamma.rval[jr] = r;
        work->mu1.rval[jr] = r;
        work->mu2.rval[jr] = r;
        work->mu3.rval[jr] = r;
        work->mu4.rval[jr] = r;
        work->mu5.rval[jr] = r;
        work->kappa1.rval[jr] = r;
        work->kappa2.rval[jr] = r;
        work->kappa3.rval[jr] = r;
        work->kappa4.rval[jr] = r;
        work->kappa5.rval[jr] = r;
        work->kappa6.rval[jr] = r;
        work->kappa7.rval[jr] = r;
        work->kappa8.rval[jr] = r;
        for (int i = 0; i < nlat; i++) {
            for (int j = 0; j < nphi; j++) {
                const double theta = acos(shtns->ct[i]);
                const double phi = j*2*M_PI/nphi;
                //int p = i*nphi+j; // SHT_PHI_CONTIGUOUS
                int p = j*nlat+i; // SHT_THETA_CONTIGUOUS
                vgrid[p] = sqr(r)*F1(work,r,theta,phi);
                dgrid[p] = r*DF2DR(work,r,theta,phi);
                for (int l = 0; l <= ALPHA_LMAX; l++)
                    kgrid[l][p] = sqr(r)*K(work,l,r,theta,phi)/(2*l+1);
                double ft = DF2DTHETA(work,r,theta,phi);
                double fp = DF2DPHI(work,r,theta,phi);
                mu1grid[p] = ft*cos(phi);
                mu3grid[p] = ft*sin(phi);
                mu5grid[p] = ft/tan(theta);
                if(!isfinite(mu5grid[p])) mu5grid[p] = 0;
                kappa1grid[p] = fp*sqr(cos(phi));
                kappa2grid[p] = fp*cos(phi)*sin(phi);
                kappa4grid[p] = fp*sqr(sin(phi));
                kappa6grid[p] = fp*cos(phi)/tan(theta);
                if(!isfinite(kappa6grid[p])) kappa6grid[p] = 0;
                kappa7grid[p] = fp*sin(phi)/tan(theta);
                if(!isfinite(kappa7grid[p])) kappa7grid[p] = 0;
                kappa8grid[p] = fp;
            }
        }

        //fedisableexcept(FE_DIVBYZERO|FE_INVALID);
        spat_to_SH(shtns, vgrid, cv);
        spat_to_SH(shtns, dgrid, cd);
        spat_to_SH(shtns, mu1grid, cmu1);
        spat_to_SH(shtns, mu3grid, cmu3);
        spat_to_SH(shtns, mu5grid, cmu5);
        spat_to_SH(shtns, kappa1grid, ckappa1);
        spat_to_SH(shtns, kappa2grid, ckappa2);
        spat_to_SH(shtns, kappa4grid, ckappa4);
        spat_to_SH(shtns, kappa6grid, ckappa6);
        spat_to_SH(shtns, kappa7grid, ckappa7);
        spat_to_SH(shtns, kappa8grid, ckappa8);
        for (int l = 0; l <= ALPHA_LMAX; l++)
            spat_to_SH(shtns, kgrid[l], ck[l]);
        //feenableexcept(FE_DIVBYZERO|FE_INVALID);

        for (int l = 0; l <= 2*NMAX; l++) {
            for (int m = 0; m <= l; m++) {
                assert(l <= ALPHA_LMAX);
                assert(m <= ALPHA_LMAX);
                assert(jr <= R_MAX_INDEX);
                work->alpha.cmplx[l][m][jr] = cv[LM(shtns,l,m)];
                work->beta.cmplx[l][m][jr] = ck[l][LM(shtns,l,m)];
                work->gamma.cmplx[l][m][jr] = cd[LM(shtns,l,m)];
                work->mu1.cmplx[l][m][jr] = cmu1[LM(shtns,l,m)];
                work->mu2.cmplx[l][m][jr] = cmu3[LM(shtns,l,m)]*r;
                work->mu3.cmplx[l][m][jr] = cmu3[LM(shtns,l,m)];
                work->mu4.cmplx[l][m][jr] = cmu1[LM(shtns,l,m)]*r;
                work->mu5.cmplx[l][m][jr] = cmu5[LM(shtns,l,m)];
                work->kappa1.cmplx[l][m][jr] = ckappa1[LM(shtns,l,m)];
                work->kappa2.cmplx[l][m][jr] = ckappa2[LM(shtns,l,m)];
                work->kappa3.cmplx[l][m][jr] = ckappa6[LM(shtns,l,m)]*r;
                work->kappa4.cmplx[l][m][jr] = ckappa4[LM(shtns,l,m)];
                work->kappa5.cmplx[l][m][jr] = ckappa7[LM(shtns,l,m)]*r;
                work->kappa6.cmplx[l][m][jr] = ckappa6[LM(shtns,l,m)];
                work->kappa7.cmplx[l][m][jr] = ckappa7[LM(shtns,l,m)];
                work->kappa8.cmplx[l][m][jr] = ckappa8[LM(shtns,l,m)]*r;
            }
        }
        //printf(".");
        //fflush(stdout);
    }
    gettimeofday(&tend, NULL);
    printf("done in %f sec\n", tdelta(&tbegin, &tend));
}

double fast_3j_prod(int l1, int l2, int lambda, int m1, int m2, int M)
{
    const int lmax = NMAX;
    const int size_l12 = lmax+1; // 0..lmax
    const int size_lambda = 2*lmax+1; // 0..2*lmax
    const int size_m12 = 2*lmax+1; // -lmax..lmax
    typedef double all[size_l12][size_l12][size_lambda][size_m12][size_m12];
    static all *f = NULL;
    if (!f) {
        printf("# begin pre-calc FAST_3J_PROD (%lu elements)... ", sizeof(all)/sizeof(double));
        fflush(stdout);
        f = malloc(sizeof(all));
        for (int l1 = 0; l1 <= lmax; l1++)
            for (int l2 = 0; l2 <= lmax; l2++)
                for (int lambda = 0; lambda <= 2*lmax; lambda++)
                    for (int m1 = -lmax; m1 <= lmax; m1++)
                        for (int m2 = -lmax; m2 <= lmax; m2++)
                            (*f)[l1][l2][lambda][m1+lmax][m2+lmax] = INT3J(l1,l2,lambda,0,0,0)*INT3J(l1,l2,lambda,m1,m2,-m1-m2);
        printf("done\n");
    }
    assert(l1 >= 0 && l1 < size_l12);
    assert(l2 >= 0 && l2 < size_l12);
    assert(lambda >= 0 && lambda < size_lambda);
    assert(m1 >= -lmax && m1 <= lmax);
    assert(m2 >= -lmax && m2 <= lmax);
    if (m1+m2 != -M) return 0;
    return (*f)[l1][l2][lambda][m1+lmax][m2+lmax];
}

double complex integrate_rpart(const struct elem *elem, const struct work *work, struct qdouble *qdouble, int madjust, int deriv)
{
    int Np = elem->bra.N;
    int lp = elem->bra.l;
    int mp = elem->bra.m;
    int sp = elem->bra.s;

    int N = elem->ket.N;
    int l = elem->ket.l;
    int m = elem->ket.m;
    int s = elem->ket.s;

    struct unl {
        double val[ALPHA_LMAX][ALPHA_LMAX][R_MAX_INDEX]; // XXX: possible
        double deriv[ALPHA_LMAX][ALPHA_LMAX][R_MAX_INDEX]; // overflow
    };

    static struct unl un, up;
    assert(qdouble->npoint <= R_MAX_INDEX);
    assert(NMAX+1 <= ALPHA_LMAX);
    static int first = 1;
    if (first) {
        first = 0;
        struct work *w = malloc(sizeof(struct work));
        for (int N = 0; N <= NMAX; N++) {
            for (int l = 0; l <= N; l++) {
                if (N%2 != l%2) continue;
                for (int k = 0; k < qdouble->npoint; k++) {
                    struct qn q = {.N = N, .l = l};
                    w->type = PROTON;
                    up.val[N][l][k] = UNL(&q, w, qdouble->rval[k]);
                    up.deriv[N][l][k] = UNL_DERIV(&q, w, qdouble->rval[k]);
                    w->type = NEUTRON;
                    un.val[N][l][k] = UNL(&q, w, qdouble->rval[k]);
                    un.deriv[N][l][k] = UNL_DERIV(&q, w, qdouble->rval[k]);
                }
            }
        }
        free(w);
    }

    double bra_ket[qdouble->npoint];
    int bkready = 0;

    double complex scmplx = 0;
    for (int lambda = abs(lp-l); lambda <= lp+l; lambda++) {
        int mdiff = abs(mp-m-madjust);
        //if (abs(mdiff)>lambda) continue;
        if (abs(m+madjust)>l) continue;
        double complex integral;

        if (!isnan(creal(cache_get(qdouble->cache, elem, lambda, mdiff)))) {
            integral = cache_get(qdouble->cache, elem, lambda, mdiff);
        } else {
            //double complex cmplx[qdouble->npoint];
            //memcpy(cmplx, qdouble->cmplx[lambda][mdiff], sizeof(cmplx));
            double complex * restrict cmplx = qdouble->cmplx[lambda][mdiff];

            if (!bkready) {
                double *bra, *ket;
                struct unl *u;
                if (work->type == PROTON)
                    u = &up;
                else if (work->type == NEUTRON)
                    u = &un;
                else
                    abort("bad type");
                bra = u->val[elem->bra.N][elem->bra.l];
                ket = (deriv ? u->deriv[elem->ket.N][elem->ket.l] : u->val[elem->ket.N][elem->ket.l]);
                for (int i = 0; i < qdouble->npoint; i++)
                    bra_ket[i] = bra[i]*ket[i];
                bkready = 1;
            }

            integral = 0;
            assert(qdouble->npoint > 1);
            const double rstep = qdouble->rval[1] - qdouble->rval[0];
            assert(rstep > 0);
            for (int k = 1; k <= qdouble->npoint-2; k+=2) {
                double complex prev, this, next;
                prev = cmplx[k-1]*bra_ket[k-1];
                this = cmplx[k]*bra_ket[k];
                next = cmplx[k+1]*bra_ket[k+1];
                integral += (rstep/3)*(prev+4*this+next);
            }
            cache_set(qdouble->cache, elem, lambda, mdiff, integral);
        }

        if (mp < m+madjust) {
            integral *= minus_one(m+madjust-mp);
            integral = conj(integral);
        }

        if (abs(m+madjust) <= l)
            scmplx += HAT(lambda)*fast_3j_prod(lp,l,lambda,-mp,m+madjust,mp-m-madjust)*integral;
    }

    double complex c = minus_one(mp)*HAT(l)*HAT(lp)/sqrt(4*PI);
    return c*scmplx;
}

double rn_matrix_element(int n, const struct elem *elem, const struct work *work)
{
    struct param {
        const struct work *work;
        const struct elem *elem;
        const int n;
    };

    double fn(double r, const struct param *param)
    {
        double wf1 = UNL(&param->elem->ket, param->work, r);
        double wf2 = UNL(&param->elem->bra, param->work, r);
        return pow(r,(param->n)+2)*wf1*wf2; // XXX: sqr(r)
    }

    static gsl_integration_cquad_workspace *workspace = NULL;
    if (!workspace)
        workspace = gsl_integration_cquad_workspace_alloc(100);

    struct param p = {.work = work, .elem = elem, .n = n};

    gsl_function f;
    f.function = (double (*)(double, void*)) fn;
    f.params = (void*) &p;
    double a = 0;
    double b = 50;
    double epsabs = 0;
    double epsrel = 1e-6;
    double result;
    double abserr;
    size_t nevals;

    gsl_integration_cquad(&f, a, b, epsabs, epsrel, workspace, &result, &abserr, &nevals);
    return result;
}

double complex matrix_element_kinetic(const struct elem *elem, const struct work *work)
{
    //print_elem(elem);
    // spin values should match
    if (elem->bra.s != elem->ket.s)
        return 0;

    // parity conservation
    if (elem->bra.l % 2 != elem->ket.l % 2)
        return 0;

    double MC2 = 0;
    if (work->type == PROTON) {
        MC2 = MC2_PROTON;
    } else if (work->type == NEUTRON) {
        MC2 = MC2_NEUTRON;
    } else {
        abort("bad type");
    }
    assert(MC2 > 0);

    int Np = elem->bra.N;
    int lp = elem->bra.l;
    int mp = elem->bra.m;
    int sp = elem->bra.s;

    int N = elem->ket.N;
    int l = elem->ket.l;
    int m = elem->ket.m;
    int s = elem->ket.s;

    double m_omega2 = sqr(hbar_omega) * MC2 / sqr(HC);
    double r2 = 0;
    if (l == lp && m == mp && s == sp)
        if (N == Np || abs(N - Np) == 2)
            r2 = rn_matrix_element(2, elem, work);
    double T = hbar_omega*(N+3./2.)*delta(N,Np)*delta(l,lp)*delta(m,mp)*delta(s,sp)-m_omega2*r2/2;
    return T;
}

void make_kinetic(enum nucleon_type type, gsl_matrix_complex *matrix)
{
    gsl_matrix_complex_set_zero(matrix);
    struct elem e;
    struct work *w = malloc(sizeof(struct work));
    w->type = type;
    for (int i1 = 0; i1 < ndim; i1++) {
        for (int i2 = 0; i2 < ndim; i2++) {
            if (i1 <= i2) continue;
            e.ket = hbasis[i1];
            e.bra = hbasis[i2];
            gsl_complex v = c99_to_gsl(matrix_element_kinetic(&e, w));
            gsl_matrix_complex_set(matrix, i1, i2, v);
            gsl_matrix_complex_set(matrix, i2, i1, gsl_complex_conjugate(v));
        }
    }
    for (int i = 0; i < ndim; i++) {
        e.ket = hbasis[i];
        e.bra = hbasis[i];
        gsl_complex v = c99_to_gsl(matrix_element_kinetic(&e, w));
        gsl_matrix_complex_set(matrix, i, i, v);
    }
    free(w);
}

double complex matrix_element_coul(struct elem *elem, struct work *work)
{
    //print_elem(elem);
    // spin values should match
    if (elem->bra.s != elem->ket.s)
        return 0;

    // parity conservation
    //if (elem->bra.l % 2 != elem->ket.l % 2)
        //return 0;

    int Np = elem->bra.N;
    int lp = elem->bra.l;
    int mp = elem->bra.m;
    int sp = elem->bra.s;

    int N = elem->ket.N;
    int l = elem->ket.l;
    int m = elem->ket.m;
    int s = elem->ket.s;
    int Z = work->Z;

    //double c = 3*(Z-1)*(HC/ALPHA_FINE_STRUCTURE)*pow(R003,-3);
    double c = 3*Z*(HC/ALPHA_FINE_STRUCTURE)*pow(R003,-3);
    double complex cc = integrate_rpart(elem, work, &work->beta, 0, 0);
    double complex v = c*cc;

    return v;
}

double complex matrix_element_nucl(struct elem *elem, struct work *work)
{
    //print_elem(elem);
    // spin values should match
    if (elem->bra.s != elem->ket.s)
        return 0;

    // parity conservation
    if (elem->bra.l % 2 != elem->ket.l % 2)
        return 0;

    double U1 = 0;
    if (work->type == PROTON) {
        U1 = U1P;
    } else if (work->type == NEUTRON) {
        U1 = U1N;
    } else {
        abort("bad type");
    }
    assert(U1 > 0);

    int Np = elem->bra.N;
    int lp = elem->bra.l;
    int mp = elem->bra.m;
    int sp = elem->bra.s;

    int N = elem->ket.N;
    int l = elem->ket.l;
    int m = elem->ket.m;
    int s = elem->ket.s;

    double complex c = integrate_rpart(elem, work, &work->alpha, 0, 0);
    double complex v = -U1*c;

    return v;
}

double complex matrix_element_1r_df2dr(struct elem *elem, struct work *work)
{
    //print_elem(elem);
    // spin values should match
    if (elem->bra.s != elem->ket.s)
        return 0;

    // parity conservation
    //if (elem->bra.l % 2 != elem->ket.l % 2)
    //    return 0;

    int Np = elem->bra.N;
    int lp = elem->bra.l;
    int mp = elem->bra.m;
    int sp = elem->bra.s;

    int N = elem->ket.N;
    int l = elem->ket.l;
    int m = elem->ket.m;
    int s = elem->ket.s;

    double complex v = integrate_rpart(elem, work, &work->gamma, 0, 0);
    return v;
}

double complex matrix_element_f(struct elem *elem, struct work *work)
{
    int Np = elem->bra.N;
    int lp = elem->bra.l;
    int mp = elem->bra.m;
    int sp = elem->bra.s;

    int N = elem->ket.N;
    int l = elem->ket.l;
    int m = elem->ket.m;
    int s = elem->ket.s;

    double complex F = 0;
    for (int jj=abs(2*l-1); jj <= 2*l+1; jj+=2) { // jj == 2*j
        double cg1 = clebsch_gordan_coeff(2*l, 2*m, 1, 2*s-1, jj, 2*m+2*s-1);
        double c = 0.5*(0.5*jj*(0.5*jj+1)-l*(l+1)-0.75);
        for (int mpp=-l; mpp<=l; mpp++) {
            for (int spp=0; spp<=1; spp++) {
                double cg2 = clebsch_gordan_coeff(2*l, 2*mpp, 1, 2*spp-1, jj, 2*m+2*s-1);
                double complex gamma = 0;

                struct elem e = *elem;
                e.ket.m = mpp;
                e.ket.s = spp;
                if (fabs(c*cg1*cg2)>0)
                    gamma = matrix_element_1r_df2dr(&e, work);
                F+=c*cg1*cg2*gamma;
            }
        }
    }

#define MU(n, deriv) integrate_rpart(elem, work, &work->mu##n, 0, deriv)
    if (s != sp) F += 0.5*m*MU(1,0);        // M1
    if (s != sp) F += -0.5*I*MU(2,1);       // M2
    if (s != sp) F += I*(s-0.5)*m*MU(3,0);  // M3
    if (s != sp) F += -(s-0.5)*MU(4,1);     // M4
    if (s == sp) F += (s-0.5)*m*MU(5,0);    // M5
#undef MU

    const double cplus = sqrt((l+m)*(l-m+1));
    const double cminus = sqrt((l-m)*(l+m+1));

#define KAPPA(n, madjust, deriv) integrate_rpart(elem, work, &work->kappa##n, madjust, deriv)
    if (s!=sp) F += -0.25*I*cplus*(KAPPA(1,-1,0)+I*KAPPA(2,-1,0));        // K1
    if (s!=sp) F += 0.25*I*cminus*(KAPPA(1,+1,0)-I*KAPPA(2,+1,0));        // K2
    if (s!=sp) F += -0.5*I*KAPPA(3,0,1);                                  // K3
    if (s!=sp) F += 0.5*(s-0.5)*cplus*(KAPPA(2,-1,0)+I*KAPPA(4,-1,0));    // K4
    if (s!=sp) F += -0.5*(s-0.5)*cminus*(KAPPA(2,+1,0)-I*KAPPA(4,+1,0));  // K5
    if (s!=sp) F += (s-0.5)*KAPPA(5,0,1);                                 // K6
    if (s==sp) F += -0.5*I*(s-0.5)*cplus*(KAPPA(6,-1,0)+I*KAPPA(7,-1,0)); // K7
    if (s==sp) F += 0.5*I*(s-0.5)*cminus*(KAPPA(6,+1,0)-I*KAPPA(7,+1,0)); // K8
    if (s==sp) F += I*(s-0.5)*KAPPA(8,0,1);                               // K9
#undef KAPPA

    return F;
}

double complex matrix_element_ls_approx(struct elem *elem, struct work *work)
{
    //print_elem(elem);
    // spin values should match
    //if (elem->bra.s != elem->ket.s)
        //return 0;

    // parity conservation
    if (elem->bra.l % 2 != elem->ket.l % 2)
        return 0;
    double complex F1 = matrix_element_f(elem, work);
    struct elem switched=*elem;
    switched.ket = elem->bra;
    switched.bra = elem->ket;
    double complex F2 = matrix_element_f(&switched, work);
    return 2*U2*(F1+conj(F2));
}

gsl_complex set_matrix_element(struct elem *e, struct work *work, struct matrix *matrix, int i, int j)
{
    double complex vnucl = matrix_element_nucl(e, work);
    double complex vls = matrix_element_ls_approx(e, work);
    double complex vcoul = 0;
    if (work->type == PROTON)
        vcoul = matrix_element_coul(e, work);
    double complex vfull = vnucl + vls + vcoul;
    gsl_matrix_complex_set(matrix->nucl, i, j, c99_to_gsl(vnucl));
    gsl_matrix_complex_set(matrix->ls, i, j, c99_to_gsl(vls));
    gsl_matrix_complex_set(matrix->coul, i, j, c99_to_gsl(vcoul));
    gsl_matrix_complex_set(matrix->full, i, j, c99_to_gsl(vfull));
    return c99_to_gsl(vfull);
}


void make_matrix(struct work *w, struct matrix *matrix)
{
    gsl_matrix_complex_set_zero(matrix->full);
    struct elem e;
    for (int i1 = 0; i1 < ndim; i1++) {
        for (int i2 = 0; i2 < ndim; i2++) {
            if (i1 < i2) continue;
            e.ket = hbasis[i1];
            e.bra = hbasis[i2];
            set_matrix_element(&e, w, matrix, i1, i2);
        }
    }
    for (int i1 = 0; i1 < ndim; i1++) {
        for (int i2 = 0; i2 < ndim; i2++) {
            if (i1 >= i2) continue;
#define set_conj(m, i, j) gsl_matrix_complex_set(m, i, j, gsl_complex_conjugate(gsl_matrix_complex_get(m, j, i)))
            set_conj(matrix->full, i1, i2);
            set_conj(matrix->nucl, i1, i2);
            set_conj(matrix->ls, i1, i2);
            set_conj(matrix->coul, i1, i2);
#undef set_conj
        }
    }
}

void diagonalization_slow(struct matrix *matrix)
{
    assert(matrix != NULL);
    assert(matrix->full != NULL);
    assert(matrix->full->size1 == matrix->full->size2);
    assert(matrix->eval != NULL);
    assert(matrix->eval->size == matrix->full->size1);
    assert(matrix->evec != NULL);
    assert(matrix->evec->size1 == matrix->evec->size2);
    assert(matrix->evec->size1 == matrix->full->size1);
    size_t ndim = matrix->full->size1;

    gsl_matrix_complex *copy = gsl_matrix_complex_alloc(ndim, ndim);
    gsl_matrix_complex_memcpy(copy, matrix->full);
    gsl_eigen_hermv_workspace *work = gsl_eigen_hermv_alloc(ndim);
    gsl_eigen_hermv(copy, matrix->eval, matrix->evec, work);
    gsl_eigen_hermv_sort(matrix->eval, matrix->evec, GSL_EIGEN_SORT_VAL_ASC);

    gsl_eigen_hermv_free(work);
    gsl_matrix_complex_free(copy);
}

double get_radius(const struct matrix *pmatrix, const struct matrix *nmatrix, int A, int Z)
{
    static void *pstorage, *nstorage;
    double (*r2p)[NMAX+1][NMAX+1], (*r2n)[NMAX+1][NMAX+1];
    static int first = 1;
    if (first) {
        first = 0;
        struct work *work = malloc(sizeof(struct work));
        struct elem *elem = malloc(sizeof(struct elem));
        r2p = malloc(sizeof(*r2p));
        r2n = malloc(sizeof(*r2n));
        for (int N = 0; N <= NMAX; N++) {
            for (int l = 0; l <= N; l++) {
                struct qn qn = {.N = N, .l = l};
                elem->bra = qn;
                elem->ket = qn;
                work->type = PROTON;
                (*r2p)[N][l] = rn_matrix_element(2, elem, work);
                work->type = NEUTRON;
                (*r2n)[N][l] = rn_matrix_element(2, elem, work);
            }
        }
        free(work);
        free(elem);
        pstorage = r2p;
        nstorage = r2n;
    }

    r2p = pstorage;
    r2n = nstorage;

    double result = 0;

    assert(pmatrix != NULL);
    assert(pmatrix->evec != NULL);
    assert(pmatrix->evec->size1 == pmatrix->evec->size2);
    const int ndim = (int) pmatrix->evec->size1;
    assert(nmatrix != NULL);
    assert(nmatrix->evec != NULL);
    assert(nmatrix->evec->size1 == nmatrix->evec->size2);
    assert((int) nmatrix->evec->size1 == ndim);

    for (int i = 0; i < Z; i++) {
        double ravg = 0;
        for (int j = 0; j < ndim; j++) {
            int N = hbasis[j].N;
            int l = hbasis[j].l;
            double a2 = gsl_complex_abs2(gsl_matrix_complex_get(pmatrix->evec, j, i));
            ravg += a2*sqrt((*r2p)[N][l]);
        }
        result += ravg/A;
    }

    for (int i = 0; i < A-Z; i++) {
        double ravg = 0;
        for (int j = 0; j < ndim; j++) {
            int N = hbasis[j].N;
            int l = hbasis[j].l;
            double a2 = gsl_complex_abs2(gsl_matrix_complex_get(nmatrix->evec, j, i));
            ravg += a2*sqrt((*r2n)[N][l]);
        }
        result += ravg/A;
    }

    return result;
}

void diagonalization_fast(struct matrix *matrix)
{
    assert(matrix != NULL);
    assert(matrix->full != NULL);
    assert(matrix->full->size1 == matrix->full->size2);
    assert(matrix->eval != NULL);
    assert(matrix->eval->size == matrix->full->size1);
    assert(matrix->evec != NULL);
    assert(matrix->evec->size1 == matrix->evec->size2);
    assert(matrix->evec->size1 == matrix->full->size1);
    int ndim = (int) matrix->full->size1;

    int nodd=0, neven=0;
    for (int i = 0; i < ndim; i++) 
        if (hbasis[i].l%2==0) 
            neven++;
        else
            nodd++;
    assert(nodd+neven==ndim);

    int iodd[nodd], ieven[neven];
    int podd=0, peven=0;
    for (int i = 0; i < ndim; i++) 
        if (hbasis[i].l%2==0) 
            ieven[peven++]=i;
        else
            iodd[podd++]=i;
    assert(podd==nodd);
    assert(peven==neven);

#define SET gsl_matrix_complex_set
#define GET gsl_matrix_complex_get
    gsl_matrix_complex *matrix_odd = gsl_matrix_complex_alloc(nodd, nodd);
    gsl_matrix_complex *evec_odd = gsl_matrix_complex_alloc(nodd, nodd);
    gsl_vector *eval_odd = gsl_vector_alloc(nodd);
    gsl_eigen_hermv_workspace *work_odd = gsl_eigen_hermv_alloc(nodd);
    for (int i1 = 0; i1 < nodd; i1++)
        for (int i2 = 0; i2 < nodd; i2++)
            SET(matrix_odd, i1, i2, GET(matrix->full, iodd[i1], iodd[i2]));
    gsl_eigen_hermv(matrix_odd, eval_odd, evec_odd, work_odd);
    
    gsl_matrix_complex *matrix_even = gsl_matrix_complex_alloc(neven, neven);
    gsl_matrix_complex *evec_even = gsl_matrix_complex_alloc(neven, neven);
    gsl_vector *eval_even = gsl_vector_alloc(neven);
    gsl_eigen_hermv_workspace *work_even = gsl_eigen_hermv_alloc(neven);
    for (int i1 = 0; i1 < neven; i1++)
        for (int i2 = 0; i2 < neven; i2++)
            SET(matrix_even, i1, i2, GET(matrix->full, ieven[i1], ieven[i2]));
    gsl_eigen_hermv(matrix_even, eval_even, evec_even, work_even);

    gsl_matrix_complex_set_zero(matrix->evec);
    for (int i = 0; i < nodd; i++) {
        gsl_vector_set(matrix->eval, i, gsl_vector_get(eval_odd, i));
        for (int j = 0; j < nodd; j++)
            SET(matrix->evec, iodd[j], i, GET(evec_odd, i, j));
    }
    for (int i = 0; i < neven; i++) {
        gsl_vector_set(matrix->eval, i+nodd, gsl_vector_get(eval_even, i));
        for (int j = 0; j < neven; j++)
            SET(matrix->evec, ieven[j], i+nodd, GET(evec_even, i, j));
    }
    gsl_eigen_hermv_sort(matrix->eval, matrix->evec, GSL_EIGEN_SORT_VAL_ASC);
#undef SET
#undef GET
    
    gsl_matrix_complex_free(matrix_odd);
    gsl_eigen_hermv_free(work_odd);
    gsl_matrix_complex_free(evec_odd);
    gsl_vector_free(eval_odd);
    gsl_matrix_complex_free(matrix_even);
    gsl_eigen_hermv_free(work_even);
    gsl_matrix_complex_free(evec_even);
    gsl_vector_free(eval_even);
}

void diagonalization_only_eigenvalues(struct matrix *matrix)
{
    assert(matrix != NULL);
    assert(matrix->full != NULL);
    assert(matrix->full->size1 == matrix->full->size2);
    assert(matrix->eval != NULL);
    assert(matrix->eval->size == matrix->full->size1);
    int ndim = (int) matrix->full->size1;

    int nodd=0, neven=0;
    for (int i = 0; i < ndim; i++) 
        if (hbasis[i].l%2==0) 
            neven++;
        else
            nodd++;
    assert(nodd+neven==ndim);

    int iodd[nodd], ieven[neven];
    int podd=0, peven=0;
    for (int i = 0; i < ndim; i++) 
        if (hbasis[i].l%2==0) 
            ieven[peven++]=i;
        else
            iodd[podd++]=i;
    assert(podd==nodd);
    assert(peven==neven);

#define SET gsl_matrix_complex_set
#define GET gsl_matrix_complex_get
    gsl_matrix_complex *matrix_odd = gsl_matrix_complex_alloc(nodd, nodd);
    gsl_vector *eval_odd = gsl_vector_alloc(nodd);
    gsl_eigen_herm_workspace *work_odd = gsl_eigen_herm_alloc(nodd);
    for (int i1 = 0; i1 < nodd; i1++)
        for (int i2 = 0; i2 < nodd; i2++)
            SET(matrix_odd, i1, i2, GET(matrix->full, iodd[i1], iodd[i2]));
    gsl_eigen_herm(matrix_odd, eval_odd, work_odd);
    
    gsl_matrix_complex *matrix_even = gsl_matrix_complex_alloc(neven, neven);
    gsl_vector *eval_even = gsl_vector_alloc(neven);
    gsl_eigen_herm_workspace *work_even = gsl_eigen_herm_alloc(neven);
    for (int i1 = 0; i1 < neven; i1++)
        for (int i2 = 0; i2 < neven; i2++)
            SET(matrix_even, i1, i2, GET(matrix->full, ieven[i1], ieven[i2]));
    gsl_eigen_herm(matrix_even, eval_even, work_even);
#undef SET
#undef GET

    for (int i = 0; i < nodd; i++) {
        gsl_vector_set(matrix->eval, i, gsl_vector_get(eval_odd, i));
    }
    for (int i = 0; i < neven; i++) {
        gsl_vector_set(matrix->eval, i+nodd, gsl_vector_get(eval_even, i));
    }
    gsl_sort_vector(matrix->eval);
    
    if (matrix->evec != NULL)
        gsl_matrix_complex_set_zero(matrix->evec);

    gsl_matrix_complex_free(matrix_odd);
    gsl_eigen_herm_free(work_odd);
    gsl_vector_free(eval_odd);
    gsl_matrix_complex_free(matrix_even);
    gsl_eigen_herm_free(work_even);
    gsl_vector_free(eval_even);
}

double get_energy(const struct matrix *matrix, int nnucl)
{
    assert(matrix != NULL);
    assert(matrix->eval != NULL);
    int ndim = (int) matrix->eval->size;
    assert(ndim > nnucl);

    double result = 0;
    for (int i = 0; i < nnucl; i++)
        result += gsl_vector_get(matrix->eval, i);
    return result;
}

void fprint_gsl_complex(FILE *f, gsl_complex z)
{
    fprintf(f, "%.16g %16.g", GSL_REAL(z), GSL_IMAG(z));
}

void save(const char *type, double beta, double gamma, const struct matrix *matrix, const gsl_vector *eval)
{
    char filename_matrix[1024];
    sprintf(filename_matrix, "nuclear_matrix_%s_%f_%f.txt", type, beta, gamma);
    /*
    FILE *f = fopen(filename_matrix, "w");
    for (int i = 0; i < ndim; i++) {
        for (int j = 0; j < ndim; j++) {
            fprint_gsl_complex(f, gsl_matrix_complex_get(matrix->full, i, j));
            fprintf(f, " ");
            fprint_gsl_complex(f, gsl_matrix_complex_get(matrix->nucl, i, j));
            fprintf(f, " ");
            fprint_gsl_complex(f, gsl_matrix_complex_get(matrix->ls, i, j));
            fprintf(f, " ");
            fprint_gsl_complex(f, gsl_matrix_complex_get(matrix->coul, i, j));
            fprintf(f, "\n");
        }
    }
    fclose(f);
    */

    char filename_vector[1024];
    sprintf(filename_vector, "eigenvalues_%s_%f_%f.txt", type, beta, gamma);
    FILE *f2 = fopen(filename_vector, "w");
    gsl_vector_fprintf(f2, eval, "%.16g");
    fclose(f2);

    /*
    sprintf(filename_vector, "eigenvectors_%s_%f_%f.txt", type, beta, gamma);
    FILE *f3 = fopen(filename_vector, "w");
    for (int i = 0; i < ndim; i++) {
        for (int j = 0; j < ndim; j++) {
            double complex v = gsl_to_c99(gsl_matrix_complex_get(matrix->evec, i, j));
            fprintf(f, "%12.6g %12.6g ", creal(v), cimag(v));
        }
        fprintf(f, "\n");
    }
    fclose(f3);
    */
}

void make_deformation(struct deform *deform)
{
    const double beta = deform->beta;
    const double gamma = deform->gamma;
    const double a0 = beta*cos(gamma);
    const double a2 = 1/sqrt(2)*beta*sin(gamma);
    const double a= sqrt(1-sqrt(5.0/(4*PI))*a0+sqrt(15.0/(2*PI))*a2);
    const double b= sqrt(1-sqrt(5.0/(4*PI))*a0-sqrt(15.0/(2*PI))*a2);
    const double c= sqrt(1+sqrt(5.0/PI)*a0);
    deform->a0 = a0;
    deform->a2 = a2;
    deform->a = a;
    deform->b= b;
    deform->c= c;
    //deform->ccc = 1-2*kccc*beta*(cos(gamma)-1);
    // 1 + k0*β*cos(γ) + k2*(1/sqrt(2))*β*sin(γ)
    // 1 + k0*β*2*(cos(γ)-1) + k2*(1/sqrt(2))*β*(2/sqrt(3))*sin(γ)
    // 1 + k0*β
    // 1 + k0*β/2 + k2*(1/sqrt(2))*β*sqrt(3)/2
    //deform->ccc = 1+kccc0*a0+kccc2*a2;
    //deform->ccc = 1+kccc0*beta*(cos(gamma)+pow(sin(gamma),16));
    //deform->ccc = 1+kccc0*beta*(cos(gamma)-sin(3*gamma)/sqrt(3));
    //const double q = 0.7;
    const double q = qccc;
    //const double p1 = -0.5;
    const double p1 = pccc;
    //double p2 = -3*(1-q);
    const double p2 = -3*(1-q)-p1;
    const double p3 = 2*(1-q);
    const double x = gamma*3/PI;
    const double f = 1+p1*x+p2*pow(x,2)+p3*pow(x,3);
    deform->ccc = 1+kccc*beta*f;
}

struct matrix *alloc_matrix(int ndim)
{
    struct matrix *result;
    result = malloc(sizeof(struct matrix));
    result->full = gsl_matrix_complex_alloc(ndim, ndim);
    result->nucl = gsl_matrix_complex_alloc(ndim, ndim);
    result->ls = gsl_matrix_complex_alloc(ndim, ndim);
    result->coul = gsl_matrix_complex_alloc(ndim, ndim);
    result->eval = gsl_vector_alloc(ndim);
    result->evec = gsl_matrix_complex_alloc(ndim, ndim);
    return result;
}

// Parameters of the Woods-Saxon potential from
// [A. J. Koning and J. P. Delaroche, Nucl. Phys. A 713, 231 (2003)].
void init(int A, int Z)
{
    printf("# A %d Z %d\n", A, Z);
    ndim = 0;
    for (int i = 0; i <= NMAX; i++)
        ndim += (i+2)*(i+1);
    printf("# ndim %d\n", ndim);
    R001 = 1.3039*pow(A, 1./3.) - 0.4054;
    alpha001 = 0.6778 - 1.487e-4*A;
    hbar_omega = 41 * pow(A, -1./3.); // [MeV]
    //double v1 = 59.3 + 21.0*(A-2.0*Z)/A - 0.024;
    double v2p = 0.007067 + 4.23e-6*A;
    double RC = 1.198*pow(A,1./3.)+0.697*pow(A,-1./3.)+12.994*pow(A,-4./3.);
    double VCP = 1.73*Z/RC;
    //U1P = v1 + VCP*v1*v2p;
    //U1N = v1;
    U1N = 59.3-21.0*(A-2.0*Z)/A-0.024*A;
    U1P = (59.3+21.0*(A-2.0*Z)/A-0.024*A)*(1+VCP*v2p);
    R003 = RC;
    U2 = 5.922 + 0.0030*A;
    R002 = 1.1854*pow(A,1./3.)-0.647;

    printf("# R001 %.16g fm\n", R001);
    printf("# alpha001 %.16g fm\n", alpha001);
    printf("# RC %.16g fm\n", RC);
    printf("# U1P %.16g MeV\n", U1P);
    printf("# U1N %.16g MeV\n", U1N);
    printf("# hbar_omega %.16g MeV\n", hbar_omega);
    printf("# U2 %.16g MeV\n", U2);
    printf("# R002 %.16g fm\n", R002);
}

// "Universal" parameter set from 
// [S. Cwiok et al., Comput. Phys. Commun. 46, 379 (1987)]
void set_univesal(int A, int Z, enum nucleon_type type)
{
    // Universal parameters of Woods-Saxon potential
    const int N = A-Z;
    const double lambda_P = 36;
    const double lambda_N = 35;
    const double V0 = 49.6;
    const double kappa = 0.86;
    const double A13 = pow(A, 1./3.);
    U1P = V0*(1+kappa*(N-Z)/(N+Z));
    U1N = V0*(1-kappa*(N-Z)/(N+Z));
    if (type == PROTON) {
        U2 = lambda_P*V0*0.25*sqr(HC/(2*MC2_PROTON));
        R001 = 1.275*A13;
        R002 = 1.20*A13;
        R003 = R001;
        alpha001 = 0.70;
        alpha002 = 0.70;
    } else if (type == NEUTRON) {
        U2 = lambda_N*V0*0.25*sqr(HC/(2*MC2_PROTON));
        R001 = 1.347*A13;
        R002 = 1.310*A13;
        R003 = R001;
        alpha001 = 0.70;
        alpha002 = 0.70;
    } else {
        abort("bad nucleon type");
    }
    printf("# universal_U1P %.16g MeV\n", U1P);
    printf("# universal_U1N %.16g MeV\n", U1N);
    printf("# universal_U2 %.16g MeV\n", U2);
    printf("# universal_R001 %.16g fm\n", R001);
    printf("# universal_R002 %.16g fm\n", R002);
    printf("# universal_alpha001,2 %.16g fm\n", alpha001);
}

struct work* alloc_work()
{
    typedef double complex CACHESIGNTR;
    size_t size = sizeof(struct work) + (3+5+8)*sizeof(cache_type);
    printf("# malloc %lu bytes\n", size);
    struct work *result = malloc(sizeof(struct work));
    result->alpha.cache = alloc_cache();
    result->beta.cache = alloc_cache();
    result->gamma.cache = alloc_cache();
    result->mu1.cache = alloc_cache();
    result->mu2.cache = alloc_cache();
    result->mu3.cache = alloc_cache();
    result->mu4.cache = alloc_cache();
    result->mu5.cache = alloc_cache();
    result->kappa1.cache = alloc_cache();
    result->kappa2.cache = alloc_cache();
    result->kappa3.cache = alloc_cache();
    result->kappa4.cache = alloc_cache();
    result->kappa5.cache = alloc_cache();
    result->kappa6.cache = alloc_cache();
    result->kappa7.cache = alloc_cache();
    result->kappa8.cache = alloc_cache();
    return result;
}

void reset_all_caches(struct work *work)
{
    reset_cache(work->alpha.cache);
    reset_cache(work->beta.cache);
    reset_cache(work->gamma.cache);
    reset_cache(work->mu1.cache);
    reset_cache(work->mu2.cache);
    reset_cache(work->mu3.cache);
    reset_cache(work->mu4.cache);
    reset_cache(work->mu5.cache);
    reset_cache(work->kappa1.cache);
    reset_cache(work->kappa2.cache);
    reset_cache(work->kappa3.cache);
    reset_cache(work->kappa4.cache);
    reset_cache(work->kappa5.cache);
    reset_cache(work->kappa6.cache);
    reset_cache(work->kappa7.cache);
    reset_cache(work->kappa8.cache);
}

void test(int argc, char *argv[])
{
    for (int i = 1; i < argc; i++)
        putenv(argv[i]);

    struct work *work = malloc(sizeof(struct work));
    work->A = 154;
    work->Z = 62;

    double beta = atof(get_param("beta"));
    double gamma = atof(get_param("gamma"));
    double r0 = atof(get_param("r0"));
    double theta0 = atof(get_param("theta0"));
    double phi0 = atof(get_param("phi0"));
    int N = atoi(get_param("N"));
    int l = atoi(get_param("l"));

    struct qn qn = {.N = N, .l = l};
    work->type = PROTON;

    work->deform.beta = beta;
    work->deform.gamma = gamma;
    make_deformation(&work->deform);

    const double dr = 0.01;
    const double dtheta = 0.1*PI/180;
    const double dphi = 0.1*PI/180;

    for (double r = 0; r <= 10; r+=0.1)
        printf("%g %g %g %g\n", r, UNL(&qn,work,r), UNL_DERIV(&qn,work,r), (UNL(&qn,work,r+dr)-UNL(&qn,work,r))/dr);
    printf("\n\n");

    for (double r = 0; r <= 10; r+=0.1)
        printf("%g %g %g %g\n", r, F2(work,r,theta0,phi0), DF2DR(work,r,theta0,phi0), (F2(work,r+dr,theta0,phi0)-F2(work,r,theta0,phi0))/dr);
    printf("\n\n");

    for (double theta = 0; theta < PI; theta+=0.05)
        printf("%g %g %g %g\n", theta, F2(work,r0,theta,phi0), DF2DTHETA(work,r0,theta,phi0), (F2(work,r0,theta+dtheta,phi0)-F2(work,r0,theta,phi0))/dtheta);
    printf("\n\n");

    for (double phi = 0; phi < 2*PI; phi+=0.05)
        printf("%g %g %g %g\n", phi, F2(work,r0,theta0,phi), DF2DPHI(work,r0,theta0,phi), (F2(work,r0,theta0,phi+dphi)-F2(work,r0,theta0,phi))/dphi);
    printf("\n\n");

    for (double theta = 0; theta < PI; theta+=0.05)
        printf("%g %g %g %g\n", theta, F2(work,r0,theta,phi0), DF2DPHI(work,r0,theta,phi0), (F2(work,r0,theta,phi0+dphi)-F2(work,r0,theta,phi0))/dphi);
    printf("\n\n");

    exit(0);
}

double jfast_int(int l1, int l2, int l3, int m1, int m2, int m3)
{
    const int lmax = NMAX;

    typedef double td[lmax+1][lmax+1][2*lmax+1][2*lmax+1][2*lmax+1][4*lmax+1];
    typedef char tc[lmax+1][lmax+1][2*lmax+1][2*lmax+1][2*lmax+1][4*lmax+1];
    static td *cache = NULL;
    static tc *flag = NULL;

    assert(l1 >= 0 && l1 <= lmax);
    assert(l2 >= 0 && l2 <= lmax);
    assert(l3 >= 0 && l3 <= 2*lmax);
    assert(m1 >= -lmax && m1 <= lmax);
    assert(m2 >= -lmax && m2 <= lmax);
    assert(m3 >= -2*lmax && m3 <= 2*lmax);

    if (!cache) {
        cache = malloc(sizeof(td));
        flag = malloc(sizeof(tc));
        memset(flag, 0, sizeof(tc));
    }

    double j;
    if (!(*flag)[l1][l2][l3][m1+lmax][m2+lmax][m3+2*lmax]) {
        j = INT3J(l1, l2, l3, m1, m2, m3);
        (*cache)[l1][l2][l3][m1+lmax][m2+lmax][m3+2*lmax] = j;
        (*flag)[l1][l2][l3][m1+lmax][m2+lmax][m3+2*lmax] = 1;
    } else {
        j = (*cache)[l1][l2][l3][m1+lmax][m2+lmax][m3+2*lmax];
    }
    return j;
}

void prepare_deform_kv(const struct matrix *matrix, int lambda, const struct work *work)
{
    assert(lambda >= 0 && lambda <= NMAX*2);
    struct timeval tbegin, tend;
    gettimeofday(&tbegin, NULL);

    printf("# begin prepare_deform_kv %d... ", lambda);
    fflush(stdout);

    double r[NMAX+1][NMAX+1][NMAX+1][NMAX+1];
    for (int N = 0; N <= NMAX; N++) {
        for (int n = 0; 2*n <= N; n++) {
            int l = N - 2*n;
            for (int Np = 0; Np <= NMAX; Np++) {
                for (int np = 0; 2*np <= Np; np++) {
                    int lp = Np - 2*np;
                    struct elem elem;
                    elem.ket = (struct qn) {.N = N, .l = l};
                    elem.bra = (struct qn) {.N = Np, .l = lp};
                    int lcond = (l+lambda >= lp && lp >= l-lambda);
                    int ncond = (N+lambda >= Np && Np >= N-lambda);
                    if (lcond && ncond)
                        r[Np][lp][N][l] = rn_matrix_element(lambda,&elem,work);
                    else
                        r[Np][lp][N][l] = 0;
                }
            }
        }
    }

    gettimeofday(&tend, NULL);
    printf("radial %f sec... ", tdelta(&tbegin, &tend));
    fflush(stdout);

    double complex u[ndim][2*lambda+1];

    for (int mu=-lambda; mu<=lambda; mu++) {
        for (int k=0; k<ndim; k++) {
            u[k][mu+lambda]=0;
            for (int i=0; i<ndim; i++) {
                for (int j=0; j<ndim; j++) {
                    if (hbasis[i].s != hbasis[j].s) continue;
                    int N =hbasis[i].N; int l =hbasis[i].l; int m =hbasis[i].m;
                    int Np=hbasis[j].N; int lp=hbasis[j].l; int mp=hbasis[j].m;
                    double rlambda = r[Np][lp][N][l];
                    if (fabs(rlambda) < 1e-20) continue;
                    gsl_complex C = gsl_matrix_complex_get(matrix->evec, i, k);
                    gsl_complex Cp = gsl_matrix_complex_get(matrix->evec, j, k);
                    double jjj1, jjj2;
                    jjj2 = jfast_int(lp, lambda, l, -mp, mu, m);
                    jjj1 = 0;
                    if (fabs(jjj2) >= 1e-20)
                        jjj1 = jfast_int(lp, lambda, l, 0, 0, 0);
                    double jprod = jjj1*jjj2;
                    if (fabs(jprod) < 1e-20) continue;
                    double complex v1 = gsl_to_c99(C)*conj(gsl_to_c99(Cp));
                    double complex v2 = 2*HAT(lambda)/sqrt(4*PI);
                    double complex v3 = minus_one(mp)*HAT(lp)*HAT(l)*rlambda*jprod;
                    u[k][mu+lambda] += v1*v2*v3;
                }
            }
        }
    }

    char fn[1024];
    const char *type = (work->type == PROTON) ? "proton" : "neutron";
    snprintf(fn, sizeof(fn)/sizeof(fn[0]), "%s-kv%d.txt", type, lambda);
    FILE *fp = fopen(fn, "w");
    if (!fp) abort("can't open file: %s\n", fn);
    for (int k=0; k<ndim; k++) {
        for (int mu=-lambda; mu<=lambda; mu++) {
            double complex v = u[k][mu+lambda];
            fprintf(fp, "%16g %16g ", creal(v), cimag(v));
        }
        fprintf(fp, "\n");
    }
    fclose(fp);
    gettimeofday(&tend, NULL);
    printf("all %f sec\n", tdelta(&tbegin, &tend));
}

int main()
{
    //shtns_verbose(1);
    fftw_import_system_wisdom();
    //feenableexcept(FE_DIVBYZERO|FE_INVALID);

    struct timeval tbegin, tend;
    const int A = atoi(get_param("A"));
    const int Z = atoi(get_param("Z"));
    setvbuf(stdout, NULL, _IOLBF, 0);

    init(A, Z);
    //test(argc, argv);
    fast_3j_prod(0,0,0,0,0,0);

    const int nbeta = atoi(get_param("nbeta"));
    const int ngamma = atoi(get_param("ngamma"));
    kccc = atof(get_param("kccc"));
    qccc = atof(get_param("qccc"));
    pccc = atof(get_param("pccc"));

    const double beta_max = 0.5;
    const double gamma_max = PI/3;

    // matrices for diagonalization
    struct matrix *pmatrix = alloc_matrix(ndim);
    struct matrix *nmatrix = alloc_matrix(ndim);

    hbasis = malloc(ndim*sizeof(struct qn));
    FILE *file_index = fopen("index.txt", "w");
    const char format[] = "%6d N:%2d n:%2d l:%2d m:%+3d s:%c1/2\n";
    for (int N = 0; N <= NMAX; N++) {
        for (int n = 0; 2*n <= N; n++) {
            int l = N - 2*n;
            for (int m = -l; m <= l; m++) {
                for (int s = 0; s <=1; s++) {
                    int i = findex(N, n, l, m, s);
                    struct qn qn = {.N = N, .l = l, .m = m, .s = s};
                    hbasis[i] = qn;
                    fprintf(file_index, format, i, N, n, l, m, s ? '+' : '-');
                }
            }
        }
    }
    fclose(file_index);

    printf("# make kinetic... ");
    fflush(stdout);
    gettimeofday(&tbegin, NULL);
    gsl_matrix_complex *pkinetic = gsl_matrix_complex_alloc(ndim, ndim);
    gsl_matrix_complex *nkinetic = gsl_matrix_complex_alloc(ndim, ndim);
    make_kinetic(PROTON, pkinetic);
    make_kinetic(NEUTRON, nkinetic);
    gettimeofday(&tend, NULL);
    printf("done in %f sec\n", tdelta(&tbegin, &tend));

    char msg[] = "beta %f gamma %f eproton %.16g eneutron %.16g eboth %.16g\n";

    struct work *work = alloc_work();
    work->A = A;
    work->Z = Z;
    int mode_kv = (nbeta == 0 && ngamma == 0);
    for (int ib = 0; ib <= nbeta; ib++) {
        for (int ig = 0; ig <= ngamma; ig++) {
            double beta, gamma;
            if (nbeta == 0)
                beta = atof(get_param("beta"));
            else
                beta = ib*beta_max/nbeta;
            if (ngamma == 0)
                gamma = atof(get_param("gamma"));
            else
                gamma = ig*gamma_max/ngamma;
            work->deform.beta = beta;
            work->deform.gamma = gamma;
            make_deformation(&work->deform);
            make_alpha_and_beta(work);

            reset_all_caches(work);

            printf("# NEUTRON matrix: ");
            fflush(stdout);
            gettimeofday(&tbegin, NULL);
            work->type = NEUTRON;
            make_matrix(work, nmatrix);
            gsl_matrix_complex_add(nmatrix->full, nkinetic);
            gettimeofday(&tend, NULL);
            printf("compute %f sec ", tdelta(&tbegin, &tend));
            fflush(stdout);

            gettimeofday(&tbegin, NULL);
            if (mode_kv)
                diagonalization_slow(nmatrix);
            else
                diagonalization_only_eigenvalues(nmatrix);
            double neutron_energy = get_energy(nmatrix, A-Z);
            gettimeofday(&tend, NULL);
            printf("diagonalize %f sec ", tdelta(&tbegin, &tend));
            fflush(stdout);
            save("neutron", beta, gamma, nmatrix, nmatrix->eval);
            printf("write\n");
            if (mode_kv) {
                prepare_deform_kv(nmatrix, 2, work);
                prepare_deform_kv(nmatrix, 4, work);
            }

            reset_all_caches(work);

            printf("# PROTON matrix: ");
            fflush(stdout);
            gettimeofday(&tbegin, NULL);
            work->type = PROTON;
            make_matrix(work, pmatrix);
            gsl_matrix_complex_add(pmatrix->full, pkinetic);
            gettimeofday(&tend, NULL);
            printf("compute %f sec ", tdelta(&tbegin, &tend));
            fflush(stdout);

            gettimeofday(&tbegin, NULL);
            if (mode_kv)
                diagonalization_slow(pmatrix);
            else
                diagonalization_only_eigenvalues(pmatrix);
            double proton_energy = get_energy(pmatrix, Z);
            gettimeofday(&tend, NULL);
            printf("diagonalize %f sec ", tdelta(&tbegin, &tend));
            save("proton", beta, gamma, pmatrix, pmatrix->eval);
            printf("write\n");

            if (mode_kv) {
                prepare_deform_kv(pmatrix, 2, work);
                prepare_deform_kv(pmatrix, 4, work);
            }

            printf(msg, beta, gamma, proton_energy, neutron_energy, proton_energy+neutron_energy);
            if (ib == 0 && ig == 0) {
                for (ig++; ig <= ngamma; ig++) {
                    gamma = ig*gamma_max/ngamma;
                    printf(msg, beta, gamma, proton_energy, neutron_energy, proton_energy+neutron_energy);
                }
                ig = ngamma+1;
            }
        }
        printf("\n");
    }
}

