CC := gcc
GSL := `gsl-config --cflags --libs` 
SHTNS_DIR := /opt/shtns
FFTW3_DIR := /usr
CFLAGS := -g -fdiagnostics-color=auto -fno-diagnostics-show-caret -std=gnu11 $(GSL) -pedantic -fwhole-program
CFLAGS += -Wconversion -Wno-sign-conversion -Wall -Wextra -Wno-unused-variable
CFLAGS += -O3 -fcx-limited-range -ffast-math -march=native -DNDEBUG

.PHONY: all

all: deform

deform: LDLIBS += -L$(SHTNS_DIR)/lib -L$(FFTW3_DIR)/lib -lfftw3 -lshtns
deform: CFLAGS += -I$(SHTNS_DIR)/include -I$(FFTW3_DIR)/include
deform: deform.c

