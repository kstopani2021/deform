% bcsplus.m: calculate pairing correction using the BCS method (full derivation)
global W ME;
basedir = fileparts(mfilename('fullpath'));
load([basedir, '/data/mass-excess-ame2016.txt'], 'ME');

global magic;
magic = [2 8 20 28 50 82 126 184 258];

function result=M1(z,n)
    MC2_PROTON = 938.2720813;
    MC2_NEUTRON = 939.5654133;
    global W;
    a = z+n;
    result = z*MC2_PROTON + n*MC2_NEUTRON - W(a,z);
    %printf('M(%d,%d) = %.12g, W = %.12g\n', a, z, result, W(a,z));
endfunction

function result=M(z,n)
    U = 931.49410242;
    global ME;
    a = z+n;
    result = a*U + ME(a,z);
    %printf('M(%d,%d) = %.12g, W = %.12g\n', a, z, result, W(a,z));
endfunction

function [n1, n2]=get_pairing_levels_independent(energies, n, distance)
    EF = energies(n);
    n1 = n;
    while (n1 > 1 && energies(n1-1) > EF-distance)
        n1 = n1 - 1;
    endwhile
    n2 = n;
    while (n2 < length(energies) && energies(n2+1) < EF+distance)
        n2 = n2 + 1;
    endwhile
endfunction

function [n1, n2]=get_pairing_levels_symmetric(energies, n, distance)
    EF = energies(n);
    n2 = n;
    while (n2 < length(energies) && energies(n2+1) < EF+distance)
        n2 = n2 + 1;
    endwhile
    n1 = n-(n2-n);
    n1 = max(n1, 1);
endfunction

function [n1, n2]=get_pairing_levels_one_shell(energies, n, distance)
    global magic;
    m2 = magic*0.5;
    i = 1;
    while (n > m2(i))
        i = i + 1;
    end
    assert(i > 1 && i < length(m2));
    n1 = m2(i-1)+1;
    n2 = m2(i);
endfunction

function [n1, n2]=get_pairing_levels_global(energies, n, distance)
    global n1_global n2_global;
    n1 = n1_global;
    n2 = n2_global;
endfunction

function [n1, n2]=get_pairing_levels(energies, n, distance)
    %[n1, n2] = get_pairing_levels_independent(energies, n, distance);
    %[n1, n2] = [1, 2*n-1];

    n1 = 1;
    n2 = 2*n-1;
    %[n1, n2] = get_pairing_levels_one_shell(energies, n, distance);
endfunction

function [n1,n2]=get_global_nrange(ntot, energies, distance)
    if (rem(ntot,2)==1)
        n0 = (ntot+1)/2;
    else
        n0 = ntot/2;
    endif
    [n1, n2] = get_pairing_levels_symmetric(energies, n0, distance);
endfunction

function [n0, energies]=get_degenerate_levels(e0, ntot)
    energies = e0(1:2:end);
    if (rem(ntot,2)==1)
        n0 = (ntot+1)/2;
    else
        n0 = ntot/2;
    endif
endfunction

function result=fbcs_iter(x)
    global N1 N2 epsk NQ delta;
    lambda=x(1);
    G=x(2);
    y=sqrt((epsk-lambda).^2+delta^2);
    result(1)=NQ-sum(1-(epsk-lambda)./y)-2*(N1-1);
    result(2)=2/G-sum(1./y);
    if(rem(NQ,2)==1)
        result(1)=result(1)-1;
    endif
endfunction

function [lambda, coupling, epc]=solve_pairing_system(e0, ntot, delta0, coupling0, distance)
    EF = e0(ntot);
    [n0, energies] = get_degenerate_levels(e0, ntot);
    [n1, n2] = get_pairing_levels(energies, n0, distance);
    ek = energies(n1:n2);

    nk=0*ek;
    nf=n0-n1+1;
    nk(1:nf)=2;
    if (rem(ntot,2)==1)
        nk(nf) = [];
        ek(nf) = [];
        n2 = n2 - 1;
    endif

    global N1 N2 epsk NQ delta;
    N1 = n1;
    N2 = n2;
    NQ = ntot;

    lambda = EF;
    delta = delta0;
    %coupling = coupling0;
    coupling = 0.2;

    epsk = ek;
    epcprev = 0;
    epc = 0;
    iter = 0;
    do
        xprev = [lambda, coupling];
        x=fsolve(@fbcs_iter, xprev);
        lambda=x(1);
        coupling=x(2);
        y=sqrt((epsk-lambda).^2+delta^2);
        vk2 = 0.5*(1-(epsk-lambda)./y);
        epsk=ek-coupling*vk2;
        epcprev=epc;
        epc1=sum((2*vk2-nk).*ek);
        epc2=delta^2/coupling;
        epc3=coupling*sum((vk2).^2);
        epc4=0.5*coupling*sum(nk);
        %epc4=0;
        epc=epc1-epc2-epc3+epc4;
        iter = iter+1;
    %until (abs((epc-epcprev)./epc) < 1e-6 && iter > 5)
    until (abs((epc-epcprev)./epc) < 1e-3)

    %epc=sum((2*vk2-nk).*ek)-delta^2/coupling-coupling*sum(vk2.^2);% + 0.5*coupling*sum(nk);
    %epc=sum((2*vk2-nk).*ek.*ff)-delta^2/coupling-coupling*sum((vk2.*ff).^2) + 0.5*coupling*sum(nk.*ff);

    %global magic;
    %if (any(magic==ntot))
        %epc = 0;
    %end
endfunction

function result=fbcs_eq1(x)
    global N1 epsk delta_exp_global NQ;
    assert(length(x)==1);
    lambda = x;
    y=sqrt((epsk-lambda).^2+delta_exp_global^2);
    result=NQ-sum(1-(epsk-lambda)./y)-2*(N1-1);
endfunction

function G=estimate_coupling_constant(e0, ntot, delta_exp, distance)
    EF = e0(ntot);
    [n0, energies] = get_degenerate_levels(e0, ntot);
    [n1, n2] = get_pairing_levels(energies, n0, distance);
    ek = energies(n1:n2);
    global N1 epsk delta_exp_global NQ;
    k = 1:length(ek);
    N1 = n1;
    N2 = n2;
    NQ = ntot;
    epsk = ek;
    delta_exp_global = delta_exp;

    lambda=fsolve(@fbcs_eq1, EF);
    y=sqrt((epsk-lambda).^2+delta_exp^2);
    G=2/sum(1./y);
endfunction

args = argv();
if (length(args) != 2)
    disp('# Usage: bcsplus <A> <Z>');
    exit;
endif

A=str2num(args{1});
Z=str2num(args{2});
N=A-Z;

printf('# bcsplus.m A %d Z %d\n', A, Z);

function delta=calc_neutron_gap(A, Z)
    N=A-Z;
    b = (1.0/8.0)*(M(Z,N+2)-4*M(Z,N+1)+6*M(Z,N)-4*M(Z,N-1)+M(Z,N-2));
    c = (1.0/4.0)*(2*(M(Z,N+1)+M(Z,N-1)+M(Z-1,N)+M(Z+1,N))-(M(Z+1,N+1)+M(Z-1,N+1)+M(Z-1,N-1)+M(Z+1,N-1))-4*M(Z,N));
    if (rem(Z, 2) == 0 && rem(N, 2) == 0)
        delta = -b;
    elseif (rem(Z, 2) == 0 && rem(N, 2) == 1)
        delta = b;
    elseif (rem(Z, 2) == 1 && rem(N, 2) == 0)
        delta = -b-c;
    elseif (rem(Z, 2) == 1 && rem(N, 2) == 1)
        delta = b+c;
    else
        disp("# fatal error");
        exit;
    endif
endfunction

function delta=calc_proton_gap(A, Z)
    N=A-Z;
    b = (1.0/8.0)*(M(Z+2,N)-4*M(Z+1,N)+6*M(Z,N)-4*M(Z-1,N)+M(Z-2,N));
    c = (1.0/4.0)*(2*(M(Z,N+1)+M(Z,N-1)+M(Z-1,N)+M(Z+1,N))-(M(Z+1,N+1)+M(Z-1,N+1)+M(Z-1,N-1)+M(Z+1,N-1))-4*M(Z,N));
    if (rem(Z, 2) == 0 && rem(N, 2) == 0)
        delta = -b;
    elseif (rem(Z, 2) == 0 && rem(N, 2) == 1)
        delta = -b-c;
    elseif (rem(Z, 2) == 1 && rem(N, 2) == 0)
        delta = b;
    elseif (rem(Z, 2) == 1 && rem(N, 2) == 1)
        delta = b+c;
    else
        disp("# fatal error");
        exit;
    endif
endfunction

neutron_gap = calc_neutron_gap(A,Z);
proton_gap = calc_proton_gap(A,Z);

printf('# proton_gap %g neutron_gap %g\n', proton_gap, neutron_gap);

distance = 5;
beta_max = 0.5;
gamma_max = pi/3;
nbeta = 30;
ngamma = 30;
printf('# distance %g MeV\n', distance);
%[global_n1_neutron, global_n2_neutron] = get_global_nrange(N, en0_deform(1:2:end), distance);
%[global_n1_proton, global_n2_proton] = get_global_nrange(Z, ep0_deform(1:2:end), distance);
for ib=0:nbeta
    for ig=0:ngamma
        try
            if (ib == 0 && ig != 0) 
                continue;
            endif
            beta = ib*beta_max/nbeta;
            gamma = ig*gamma_max/ngamma;
            en0 = load(sprintf('eigenvalues_neutron_%f_%f.txt', abs(beta), gamma));
            ep0 = load(sprintf('eigenvalues_proton_%f_%f.txt', abs(beta), gamma));
            coupling_neutron = estimate_coupling_constant(en0, N, neutron_gap, distance);
            coupling_proton = estimate_coupling_constant(ep0, Z, proton_gap, distance);
            printf('# coupling %g proton %g neutron\n', coupling_proton, coupling_neutron);
            %global n1_global n2_global;
            %n1_global = global_n1_neutron;
            %n2_global = global_n2_neutron;
            [ln cn EPCN] = solve_pairing_system(en0, N, neutron_gap, coupling_neutron, distance);
            EN = sum(en0(1:N)) + EPCN;
            %n1_global = global_n1_proton;
            %n2_global = global_n2_proton;
            [lp cp EPCP] = solve_pairing_system(ep0, Z, proton_gap, coupling_proton, distance);
            EP = sum(ep0(1:Z)) + EPCP;
            printf('beta %f gamma %f dn %f dp %f EN %g EP %g E %.12g EPCN %.12g EPCP %.12g\n', beta, gamma, 0, 0, EN, EP, EN+EP, EPCN, EPCP);
            if (ib == 0 && ig == 0)
                for ig=1:ngamma
                    gamma = ig*gamma_max/ngamma;
                    printf('beta %f gamma %f dn %f dp %f EN %g EP %g E %.12g EPCN %.12g EPCP %.12g\n', beta, gamma, 0, 0, EN, EP, EN+EP, EPCN, EPCP);
                endfor
            endif
        catch
            printf('# beta %f gamma %f not found\n', beta, gamma)
        end_try_catch
    endfor
    printf('\n');
endfor
