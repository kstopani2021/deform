Description of provided programs
================================

1) deform.c: calculation of single-particle levels in spheroidally
deformed Woods-Saxon potential.

Requirements:

* C99 compiler (tested with GCC 4.9.2 on Debian Linux 8.11 and 
  GCC 8.3.0 on Mac OS X 10.11);
* GNU Make (tested with GNU Make 4.0);
* GNU Scientific Library (tested with GSL 1.16);
* FFTW library (tested with FFTW 3.3.4);
* SHTNS library (tested with SHTNS 3.2.2-r675 from 
  https://nschaeff.bitbucket.io/shtns).

For compilation specify install location of SNTNS in the makefile 
and run the "make" command.

Usage:

$ Z=38 A=90 nbeta=30 ngamma=30 kccc=-0.018 qccc=0.8 pccc=-0.4 ./deform

"Z" and "A" are the charge and the mass numbers, "nbeta" and "ngamma"
specify the number of intervals of the grid of deformation parameters,
and "kccc", "qccc", "pccc" are, respectively, the k, q, and p1
parameters of the model.

Description of output:

For each point of the (beta,gamma) grid the total single-particle
energy is shown, and individual single-particle energies are saved in
"eigenvalues_proton" and "eigenvalues_neutron" text files.

2) bcsplus.m: calculation of pairing correction using the BCS method

Requirements:

* GNU Octave (tested with GNU Octave 3.8.2).

Usage:

$ octave -q bcsplus.m 90 38

I.e., the first parameter specifies the mass number A, the second
parameter specifies the charge number Z. The command has to be run in
the same directory where the DEFORM program has previously saved the
"eigenvalue_*.txt" files.

Description of output:

For each deformation the EPCP and EPCN pairing corrections for,
respectively, protons and neutrons, as well as the corrected energy E
are shown.

